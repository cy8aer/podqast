import httpretty

from podcast.feedutils import fetch_feed, NewFeedUrlError
from test import read_testdata, xml_headers


@httpretty.activate(allow_net_connect=False)
def test_new_feed_url():
    url_f = "https://fakefeed.com/"
    httpretty.HTTPretty.register_uri(httpretty.HTTPretty.GET, url_f, body=read_testdata('testdata/new-feed-url-feed.rss'),
                                     adding_headers=xml_headers)
    try:
        fetch_feed(None,url_f)
        assert False
    except NewFeedUrlError:
        pass
