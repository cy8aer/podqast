"""
Test the Cache class
"""

import sys
sys.path.append("../python")

from podcast.cache import Cache

cache = None
    
def test_store():
    """
    Test the cache store function
    """

    cache = Cache()
    len1 = cache.size
    cache.store("test1", 1)
    len2 = cache.size

    assert len1 == 0
    assert len2 == len1 + 1

def test_delete():
    """
    Test cache delete of element
    """

    cache = Cache()
    cache.store("test1", 1)
    cache.store("test2", 2)
    len1 = cache.size
    cache.delete("test2")
    len2 = cache.size
    cache.delete("test2")
    len3 = cache.size
    
    assert len1 == 2
    assert len2 == len3 == 1

def test_exists():
    """
    does an cache element exist
    """

    cache = Cache()
    cache.store("test1", 1)
    cache.store("test2", 2)
    
    assert cache.exists("test1") == True
    assert cache.exists("test2") == True
    assert cache.exists("test3") == False

def test_cachelimit():
    """
    does the cache store more elements than limit?
    """

    cache = Cache(limit=2)
    cache.store("test1", 1)
    cache.store("test2", 2)
    cache.store("test3", 3)

    assert cache.size == 2
    assert cache.exists("test1") == False   # fifo
    assert cache.exists("test2") == True
    assert cache.exists("test3") == True

def test_get():
    """
    can we get an element from cache?
    """

    cache = Cache()
    cache.store("test1", 1)
    cache.store("test2", 2)

    t1 = cache.get("test1")
    t2 = cache.get("test2")

    assert t1 == 1
    assert t2 == 2
    
def test_deleteemptystore():
    """
    This should do nothing - we do not need an exception
    """

    cache = Cache()
    t = cache.get("test1")

    assert cache.size == 0
    assert t == None
