import asyncio
import inspect
import logging
from functools import wraps


def wrap_async(fun):
    @wraps(fun)
    def inner(*args, **kwargs):
        asyncio.run(fun(*args, **kwargs))

    return inner

class asnywrapped():

    def __init__(self):
        print("abc!!")
        logging.info("mem"+str(inspect.getmembers(self, inspect.iscoroutinefunction)))
        # create for all private async functions a public non async function
        for _, fun in inspect.getmembers(self, inspect.iscoroutinefunction):
            name = fun.__name__
            if name.startswith("__"):
                logging.info(f"Creating wrapper {name[2:]}for {name}")
                self.__setattr__(name[2:], fun)

    async def __testme(self):
        return True

def test_wrapper():
    print("abc!!def")
    assert asnywrapped().testme()