��aL      �podcast.podpost��Podpost���)��}�(�isaudio���position�K �state�K �
percentage�K �favorite���podurl��-https://feeds.metaebene.me/forschergeist/opus��logo_url��r/home/nemo/.local/share/harbour-podqast/icons/dafb0dd2617b0b11b4d6ec8f8a4787972854eccfd1efdcd920708e0abe86775c.jpg��	logo_path�h�insert_date�N�	published�GAס�   �title��FG078 Angst und Zukunft��link��9https://forschergeist.de/podcast/fg078-angst-und-zukunft/��	plainpart�X�  Längst nicht erst seit dem Ausbruch der Corona-Krise gilt: Wir leben in einer
Zeit, die von Unsicherheiten geprägt ist. Eine Zeit, in der gewaltige
Herausforderungen auf eine Antwort warten – während sich unsere Gesellschaft
grundlegend wandelt. Und Ungewissheit befeuert Ängste, die keineswegs nur
verborgen im Privaten gedeihen. Wenn die Furcht zu mächtig wird, kann auf
einmal ein System zu kippen drohen. Dr. Jan Kalbitzer (Jahrgang 1978),
Facharzt für Psychiatrie und Psychotherapie, hat sich während seiner
Forschungslaufbahn in Kopenhagen, Oxford und an der Berliner Charité mit
Fragen der psychischen Gesundheit auch in gesellschaftlichen Zusammenhängen
auseinandergesetzt. Da ging es etwa um die Frage, ob es so etwas wie
Internetsucht überhaupt gibt. Immer wieder nutzen Wissenschaftler ihre
Reputation, um das, was sie für moralisch richtig halten, durchzusetzen. Ein
anderes Beispiel ist die Diskussionskultur auf Online-Plattformen: Viele
Menschen haben sich aus dem öffentlichen Diskurs ausgeklinkt, aus Sorge vor
Überwachung oder Hassattacken. Der Rückzug aus einer unangenehmen oder
bedrohlichen Situation hat durchaus eine Schutzfunktion, um wieder Energie zu
tanken. Doch wenn man sich zu lange der Möglichkeit entzieht, auch positive
Rückmeldungen zu erhalten, kann man leicht in eine Depression abrutschen. Wir
brauchen also Nischen, in denen sich Menschen geschützt fühlen, damit sie sich
engagieren und ihre Kompetenzen einbringen können. Wenn gerade in der eigenen
Umgebung Spielräume offen stehen, um etwas zu verändern, lässt sich die
Lähmung überwinden. Angst bietet immer auch die Chance, die Zukunft
anzupacken. Das Gespräch wurde Ende Februar 2020 in Berlin aufgezeichnet.

��htmlpart�X~?  <p class="episode_subtitle"><strong><em>Psychisch gesund und handlungsfähig bleiben in einer sich ändernden Welt</em></strong></p>

            <p class="episode_meta">
                     
                    
</p>

            
<div style="float:right; margin-left: 10px;">
<table>
<colgroup>
    <col width="128px" />
        </colgroup>
<tr>
    <td style="vertical-align: top; width: 128px">
                            
<img alt="Jan Kalbitzer" title="Jan Kalbitzer" width="128" height="128" src="https://forschergeist.de/wp-content/cache/podlove/32/6439ea0708a45a6b2e81e22fc52639/jan-kalbitzer_128x128.jpg" srcset="https://forschergeist.de/wp-content/cache/podlove/32/6439ea0708a45a6b2e81e22fc52639/jan-kalbitzer_128x128.jpg 1x, https://forschergeist.de/wp-content/cache/podlove/32/6439ea0708a45a6b2e81e22fc52639/jan-kalbitzer_256x256.jpg 2x, https://forschergeist.de/wp-content/cache/podlove/32/6439ea0708a45a6b2e81e22fc52639/jan-kalbitzer_384x384.jpg 3x"/>

        <br/>
<strong>Jan Kalbitzer</strong>
                                                            <br/>                <a target="_blank" title="Website" href="https://www.kalbitzer.org">
                                    
<img class="podlove-contributor-button" alt="Website" width="32" height="32" src="https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_32x32.png" srcset="https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_32x32.png 1x, https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_64x64.png 2x, https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_96x96.png 3x"/>

                   </a>
                            </td>
            </tr>
    </table> 
</div>

            <p class="episode-summary">Längst nicht erst seit dem Ausbruch der Corona-Krise gilt: Wir leben in einer Zeit, die von Unsicherheiten geprägt ist. Eine Zeit, in der gewaltige Herausforderungen auf eine Antwort warten – während sich unsere Gesellschaft grundlegend wandelt. Und Ungewissheit befeuert Ängste, die keineswegs nur verborgen im Privaten gedeihen. Wenn die Furcht zu mächtig wird, kann auf einmal ein System zu kippen drohen.<br />
<br />
Dr. Jan Kalbitzer (Jahrgang 1978), Facharzt für Psychiatrie und Psychotherapie, hat sich während seiner Forschungslaufbahn in Kopenhagen, Oxford und an der Berliner Charité mit Fragen der psychischen Gesundheit auch in gesellschaftlichen Zusammenhängen auseinandergesetzt. Da ging es etwa um die Frage, ob es so etwas wie Internetsucht überhaupt gibt. Immer wieder nutzen Wissenschaftler ihre Reputation, um das, was sie für moralisch richtig halten, durchzusetzen.<br />
<br />
Ein anderes Beispiel ist die Diskussionskultur auf Online-Plattformen: Viele Menschen haben sich aus dem öffentlichen Diskurs ausgeklinkt, aus Sorge vor Überwachung oder Hassattacken. Der Rückzug aus einer unangenehmen oder bedrohlichen Situation hat durchaus eine Schutzfunktion, um wieder Energie zu tanken. Doch wenn man sich zu lange der Möglichkeit entzieht, auch positive Rückmeldungen zu erhalten, kann man leicht in eine Depression abrutschen.<br />
<br />
Wir brauchen also Nischen, in denen sich Menschen geschützt fühlen, damit sie sich engagieren und ihre Kompetenzen einbringen können. Wenn gerade in der eigenen Umgebung Spielräume offen stehen, um etwas zu verändern, lässt sich die Lähmung überwinden. Angst bietet immer auch die Chance, die Zukunft anzupacken.<br />
<br />
Das Gespräch wurde Ende Februar 2020 in Berlin aufgezeichnet.</p>





 <!-- is not feed -->


<hr style="clear: both;" />


<h2>Shownotes</h2>

<div class='osfx-shownote-block'>

    <div class="links">

    </div>

    

<div class='glossary'>
            <h3>Glossar</h3>
    <ul>
    

<li><span style="display: inline-block" class="glossary"> Uniklinik Kopenhagen</li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/S%C3%B8ren_Kierkegaard" alt="Søren Kierkegaard – Wikipedia" title="Søren Kierkegaard – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/S%C3%B8ren_Kierkegaard">Søren Kierkegaard – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Positronen-Emissions-Tomographie" alt="Positronen-Emissions-Tomographie – Wikipedia" title="Positronen-Emissions-Tomographie – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Positronen-Emissions-Tomographie">Positronen-Emissions-Tomographie – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"> MNI Standard Space</li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Charit%C3%A9" alt="Charité – Wikipedia" title="Charité – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Charit%C3%A9">Charité – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Voxel" alt="Voxel – Wikipedia" title="Voxel – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Voxel">Voxel – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Magnetresonanztomographie" alt="Magnetresonanztomographie – Wikipedia" title="Magnetresonanztomographie – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Magnetresonanztomographie">Magnetresonanztomographie – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Eugen_Bleuler" alt="Eugen Bleuler – Wikipedia" title="Eugen Bleuler – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Eugen_Bleuler">Eugen Bleuler – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Schizophrenie" alt="Schizophrenie – Wikipedia" title="Schizophrenie – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Schizophrenie">Schizophrenie – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Leisure_Suit_Larry" alt="Leisure Suit Larry – Wikipedia" title="Leisure Suit Larry – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Leisure_Suit_Larry">Leisure Suit Larry – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Aufmerksamkeitsdefizit-/Hyperaktivit%C3%A4tsst%C3%B6rung" alt="Aufmerksamkeitsdefizit-/Hyperaktivitätsstörung – Wikipedia" title="Aufmerksamkeitsdefizit-/Hyperaktivitätsstörung – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Aufmerksamkeitsdefizit-/Hyperaktivit%C3%A4tsst%C3%B6rung">Aufmerksamkeitsdefizit-/Hyperaktivitätsstörung – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Hans-Bredow-Institut" alt="Hans-Bredow-Institut – Wikipedia" title="Hans-Bredow-Institut – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Hans-Bredow-Institut">Hans-Bredow-Institut – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://kw.uni-paderborn.de/institut-fuer-medienwissenschaften/personal-a-z/personen/?tx_upbperson_personsite%5BpersonId%5D=65695&tx_upbperson_personsite%5Bcontroller%5D=Person&cHash=ba402dfe19830081aa6cc9a60800c074" alt="Prof. Dr. Tobias Matzner - Kontakt (Universität Paderborn)" title="Prof. Dr. Tobias Matzner - Kontakt (Universität Paderborn)" /> <a href="https://kw.uni-paderborn.de/institut-fuer-medienwissenschaften/personal-a-z/personen/?tx_upbperson_personsite%5BpersonId%5D=65695&tx_upbperson_personsite%5Bcontroller%5D=Person&cHash=ba402dfe19830081aa6cc9a60800c074">Prof. Dr. Tobias Matzner - Kontakt (Universität Paderborn)</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Dopamin" alt="Dopamin – Wikipedia" title="Dopamin – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Dopamin">Dopamin – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Moralische_Panik" alt="Moralische Panik – Wikipedia" title="Moralische Panik – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Moralische_Panik">Moralische Panik – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/HIV" alt="HIV – Wikipedia" title="HIV – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/HIV">HIV – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Daimler_und_Benz_Stiftung" alt="Daimler und Benz Stiftung – Wikipedia" title="Daimler und Benz Stiftung – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Daimler_und_Benz_Stiftung">Daimler und Benz Stiftung – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/SARS-CoV-2" alt="SARS-CoV-2 – Wikipedia" title="SARS-CoV-2 – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/SARS-CoV-2">SARS-CoV-2 – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Schweigespirale" alt="Schweigespirale – Wikipedia (??? Stoischew)" title="Schweigespirale – Wikipedia (??? Stoischew)" /> <a href="https://de.wikipedia.org/wiki/Schweigespirale">Schweigespirale – Wikipedia (??? Stoischew)</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Greta_Thunberg" alt="Greta Thunberg – Wikipedia" title="Greta Thunberg – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Greta_Thunberg">Greta Thunberg – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Schwammstadt" alt="Schwammstadt – Wikipedia" title="Schwammstadt – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Schwammstadt">Schwammstadt – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Filterblase" alt="Filterblase – Wikipedia" title="Filterblase – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Filterblase">Filterblase – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Randolph_M._Nesse" alt="Randolph M. Nesse – Wikipedia" title="Randolph M. Nesse – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Randolph_M._Nesse">Randolph M. Nesse – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Emmanuel_Levinas" alt="Emmanuel Levinas – Wikipedia" title="Emmanuel Levinas – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Emmanuel_Levinas">Emmanuel Levinas – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Arabischer_Fr%C3%BChling" alt="Arabischer Frühling – Wikipedia" title="Arabischer Frühling – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Arabischer_Fr%C3%BChling">Arabischer Frühling – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Discord_(Software)" alt="Discord (Software) – Wikipedia" title="Discord (Software) – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Discord_(Software)">Discord (Software) – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Tabu" alt="Tabu – Wikipedia" title="Tabu – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Tabu">Tabu – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Sascha_Lobo" alt="Sascha Lobo – Wikipedia" title="Sascha Lobo – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Sascha_Lobo">Sascha Lobo – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Antisemitismus" alt="Antisemitismus – Wikipedia" title="Antisemitismus – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Antisemitismus">Antisemitismus – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Christian_Brandes_(Autor)" alt="Christian Brandes (Autor) – Wikipedia" title="Christian Brandes (Autor) – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Christian_Brandes_(Autor)">Christian Brandes (Autor) – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://www.welt.de/debatte/kommentare/plus181099620/AfD-Rettet-Frauke-Petry-holt-sie-zurueck-in-die-Mitte.html" alt="AfD: Rettet Frauke Petry – holt sie zurück in die Mitte! - WELT" title="AfD: Rettet Frauke Petry – holt sie zurück in die Mitte! - WELT" /> <a href="https://www.welt.de/debatte/kommentare/plus181099620/AfD-Rettet-Frauke-Petry-holt-sie-zurueck-in-die-Mitte.html">AfD: Rettet Frauke Petry – holt sie zurück in die Mitte! - WELT</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=http://holgerarppe.de/v3/2018/08/14/sekte-deutschland-tut-busse-und-kehrt-um/" alt="Sekte Deutschland – Tut Buße und kehrt um! – HolgerArppe" title="Sekte Deutschland – Tut Buße und kehrt um! – HolgerArppe" /> <a href="http://holgerarppe.de/v3/2018/08/14/sekte-deutschland-tut-busse-und-kehrt-um/">Sekte Deutschland – Tut Buße und kehrt um! – HolgerArppe</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Fehlerkultur" alt="Fehlerkultur – Wikipedia" title="Fehlerkultur – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Fehlerkultur">Fehlerkultur – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Neugier" alt="Neugier – Wikipedia" title="Neugier – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Neugier">Neugier – Wikipedia</a></li>

    </ul>
    
    
    </div>
    
    
</div>



<hr style="clear: both;" />

    
<div class="podlove_cc_license">
<img src="https://forschergeist.de/wp-content/plugins/podlove-podcasting-plugin-for-wordpress/images/cc/10_1.png" alt="License" />
<p>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0">Creative Commons Attribution-ShareAlike 4.0 International License</a>
</p>
</div>��author��'Metaebene Personal Media - Tim Pritlove��id��@6041cf0c24667a2cf8cec15adc2beb6ee776aeda92ac2a213842eeabeee54e2d��length��50576987��type��
audio/opus��href��Uhttps://forschergeist.de/podlove/file/2189/s/feed/c/opus/fg078-angst-und-zukunft.opus��	file_path�N�duration�MB�chapters�]�(�
feedparser��FeedParserDict���)��(�start��00:00:00.000��title��Intro��start_parsed��datetime��	timedelta���K K K ��R�uh))��(�start��00:00:43.275��title��Begrüßung�h/h2K K+J82 ��R�uh))��(�start��00:01:31.051��title��Persönlicher Hintergrund�h/h2K K[M8Ǉ�R�uh))��(�start��00:25:31.152��title��Praktische Arbeit�h/h2K M�J�Q ��R�uh))��(�start��00:37:24.753��title��Arbeit mit Stiftungen�h/h2K M�Jh} ��R�uh))��(�start��00:42:34.451��title��Leben mit Angst�h/h2K M�	J�� ��R�uh))��(�start��00:58:11.758��title��Nischen als Chance�h/h2K M�J� ��R�uh))��(�start��01:07:07.759��title��Diskurse�h/h2K M�Jؔ ��R�uh))��(�start��01:21:50.168��title��Privilegienproblematik�h/h2K M.J@� ��R�uh))��(�start��01:40:54.697��title��Ausklang�h/h2K M�J��
 ��R�ueub.