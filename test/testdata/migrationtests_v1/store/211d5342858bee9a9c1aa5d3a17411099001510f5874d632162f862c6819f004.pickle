���Q      �podcast.podpost��Podpost���)��}�(�isaudio���position�K �state�K �
percentage�K �favorite���podurl��-https://feeds.metaebene.me/forschergeist/opus��logo_url��r/home/nemo/.local/share/harbour-podqast/icons/dafb0dd2617b0b11b4d6ec8f8a4787972854eccfd1efdcd920708e0abe86775c.jpg��	logo_path�h�insert_date�N�	published�GA�d��   �title��%FG072 Verantwortung in der Informatik��link��Ghttps://forschergeist.de/podcast/fg072-verantwortung-in-der-informatik/��	plainpart�XR  Informatik – ein Fach nur für Nerds? Keinesfalls, denn der Code, den
Programmierer schreiben, existiert ja nicht im luftleeren Raum. Software
bezieht sich letzten Endes immer auf den Menschen, interagiert mit ihm,
beeinflusst das soziale Leben. Und mittlerweile ist auch an technisch
geprägten Fakultäten angekommen, dass Informatik eine hohe gesellschaftliche
Relevanz besitzt. Peter Purgathofer lehrt an der TU Wien am Institut für
Visual Computing. Der 56-Jährige tritt dafür ein, dass Software-Entwickler
sich der Verantwortung bewusst sind, die ihre Arbeit hat. Denn Informatik wird
zunehmend zur zentralen Disziplin schlechthin, ja sogar zum Betriebssystem
unserer Gesellschaft. Tracking und Werbenetzwerke fördern eine Mediennutzung,
bei der aufmerksamkeitsheischendes Clickbate mehr zählt als inhaltliche Tiefe
und Seriosität. Automatisierte Entscheidungen können katastrophale Folgen
haben, wie etwa bei tödlichen Unfällen autonom fahrender Autos oder den
Abstürzen der Boeing 737 Max. Algorithmen sind eben keine Lösung für alles,
denn wie zuverlässig sind eigentlich die Datengrundlagen, auf denen sie
aufsetzen? Purgathofer hat deshalb an seiner Hochschule einen Einführungskurs
für Studienanfänger entwickelt. Dabei geht es darum, der nächsten
Informatikergeneration fundamentales Metawissen zu vermitteln und sie damit zu
befähigen, mit einem tieferen Verständnis durch ihr Studium zu navigieren: Wie
sieht die Wissenschaft auf die Welt, mit welchen Denkweisen wird Wissen
geschaffen? Was sind die Konsequenzen und wo liegen die Grenzen? Es wird klar:
Das Leben lässt sich nicht nur mit Einsen und Nullen erklären, es entzieht
sich immer wieder der Berechenbarkeit. Informatik ist eng mit
Sozialwissenschaften, Philosophie und Psychologie verwoben – und Problemlösung
eben nicht nur eine technische Frage. Purgathofer kritisiert nebenbei auch die
mangelnde Offenheit der vor allem unternehmensgetriebenen Forschung im Bereich
Künstlicher Intelligenz. Diese Closed Science hat für die Wissenschaft als
Ganzes schädliche Effekte, die verblüffend an die Zeit der Alchimisten
erinnern.

��htmlpart�X�?  <p class="episode_subtitle"><strong><em>Ein Einführungskurs für Informatiker vermittelt unterschiedliche Denkweisen die gesellschaftliche Bedeutung des Fachs zu vermitteln</em></strong></p>

            <p class="episode_meta">
                     
                    
</p>

            
<div style="float:right; margin-left: 10px;">
<table>
<colgroup>
    <col width="128px" />
        </colgroup>
<tr>
    <td style="vertical-align: top; width: 128px">
                            
<img alt="Peter Purgathofer" title="Peter Purgathofer" width="128" height="128" src="https://forschergeist.de/wp-content/cache/podlove/65/9c186f28b84fb7422c838cbbfc1bfe/peter-purgathofer_128x128.jpg" srcset="https://forschergeist.de/wp-content/cache/podlove/65/9c186f28b84fb7422c838cbbfc1bfe/peter-purgathofer_128x128.jpg 1x, https://forschergeist.de/wp-content/cache/podlove/65/9c186f28b84fb7422c838cbbfc1bfe/peter-purgathofer_256x256.jpg 2x, https://forschergeist.de/wp-content/cache/podlove/65/9c186f28b84fb7422c838cbbfc1bfe/peter-purgathofer_384x384.jpg 3x"/>

        <br/>
<strong>Peter Purgathofer</strong>
                                                            <br/>                <a target="_blank" title="Twitter" href="https://twitter.com/peterpur">
                                    
<img class="podlove-contributor-button" alt="Twitter" width="32" height="32" src="https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_32x32.png" srcset="https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_32x32.png 1x, https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_64x64.png 2x, https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_96x96.png 3x"/>

                   </a>
                                                            <a target="_blank" title="Website" href="http://www.piglab.org/peterpurgathofer">
                                    
<img class="podlove-contributor-button" alt="Website" width="32" height="32" src="https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_32x32.png" srcset="https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_32x32.png 1x, https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_64x64.png 2x, https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_96x96.png 3x"/>

                   </a>
                            </td>
            </tr>
    </table> 
</div>

            <p class="episode-summary">Informatik – ein Fach nur für Nerds? Keinesfalls, denn der Code, den Programmierer schreiben, existiert ja nicht im luftleeren Raum. Software bezieht sich letzten Endes immer auf den Menschen, interagiert mit ihm, beeinflusst das soziale Leben. Und mittlerweile ist auch an technisch geprägten Fakultäten angekommen, dass Informatik eine hohe gesellschaftliche Relevanz besitzt.<br />
<br />
Peter Purgathofer lehrt an der TU Wien am Institut für Visual Computing. Der 56-Jährige tritt dafür ein, dass Software-Entwickler sich der Verantwortung bewusst sind, die ihre Arbeit hat. Denn Informatik wird zunehmend zur zentralen Disziplin schlechthin, ja sogar zum Betriebssystem unserer Gesellschaft. Tracking und Werbenetzwerke fördern eine Mediennutzung, bei der aufmerksamkeitsheischendes Clickbate mehr zählt als inhaltliche Tiefe und Seriosität. Automatisierte Entscheidungen können katastrophale Folgen haben, wie etwa bei tödlichen Unfällen autonom fahrender Autos oder den Abstürzen der Boeing 737 Max. Algorithmen sind eben keine Lösung für alles, denn wie zuverlässig sind eigentlich die Datengrundlagen, auf denen sie aufsetzen?<br />
<br />
Purgathofer hat deshalb an seiner Hochschule einen Einführungskurs für Studienanfänger entwickelt. Dabei geht es darum, der nächsten Informatikergeneration fundamentales Metawissen zu vermitteln und sie damit zu befähigen, mit einem tieferen Verständnis durch ihr Studium zu navigieren: Wie sieht die Wissenschaft auf die Welt, mit welchen Denkweisen wird Wissen geschaffen? Was sind die Konsequenzen und wo liegen die Grenzen? Es wird klar: Das Leben lässt sich nicht nur mit Einsen und Nullen erklären, es entzieht sich immer wieder der Berechenbarkeit. Informatik ist eng mit Sozialwissenschaften, Philosophie und Psychologie verwoben – und Problemlösung eben nicht nur eine technische Frage. Purgathofer kritisiert nebenbei auch die mangelnde Offenheit der vor allem unternehmensgetriebenen Forschung im Bereich Künstlicher Intelligenz. Diese Closed Science hat für die Wissenschaft als Ganzes schädliche Effekte, die verblüffend an die Zeit der Alchimisten erinnern.</p>





 <!-- is not feed -->


<hr style="clear: both;" />


<h2>Shownotes</h2>

<div class='osfx-shownote-block'>

    <div class="links">

    </div>

    

<div class='glossary'>
            <h3>Glossar</h3>
    <ul>
    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Informatik" alt="Informatik – Wikipedia" title="Informatik – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Informatik">Informatik – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Elektronische_Stimmabgabe" alt="Elektronische Stimmabgabe – Wikipedia" title="Elektronische Stimmabgabe – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Elektronische_Stimmabgabe">Elektronische Stimmabgabe – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://papierwahl.at/" alt="papierwahl.at – Kritik an E-Voting" title="papierwahl.at – Kritik an E-Voting" /> <a href="https://papierwahl.at/">papierwahl.at – Kritik an E-Voting</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Soziales_Netzwerk_(Internet)" alt="Soziales Netzwerk (Internet) – Wikipedia" title="Soziales Netzwerk (Internet) – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Soziales_Netzwerk_(Internet)">Soziales Netzwerk (Internet) – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Maschinelles_Lernen" alt="Maschinelles Lernen – Wikipedia" title="Maschinelles Lernen – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Maschinelles_Lernen">Maschinelles Lernen – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz" alt="Künstliche Intelligenz – Wikipedia" title="Künstliche Intelligenz – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/K%C3%BCnstliche_Intelligenz">Künstliche Intelligenz – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://wot.pubpub.org/" alt="Ways of Thinking in Informatics" title="Ways of Thinking in Informatics" /> <a href="https://wot.pubpub.org/">Ways of Thinking in Informatics</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://vimeo.com/337454681" alt="Ways of Thinking in Informatics on Vimeo" title="Ways of Thinking in Informatics on Vimeo" /> <a href="https://vimeo.com/337454681">Ways of Thinking in Informatics on Vimeo</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Alchemie" alt="Alchemie – Wikipedia" title="Alchemie – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Alchemie">Alchemie – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Porzellan" alt="Porzellan – Wikipedia" title="Porzellan – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Porzellan">Porzellan – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Schie%C3%9Fpulver" alt="Schießpulver – Wikipedia" title="Schießpulver – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Schie%C3%9Fpulver">Schießpulver – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Johannes_Kepler" alt="Johannes Kepler – Wikipedia" title="Johannes Kepler – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Johannes_Kepler">Johannes Kepler – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Isaac_Newton" alt="Isaac Newton – Wikipedia" title="Isaac Newton – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Isaac_Newton">Isaac Newton – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Patent" alt="Patent – Wikipedia" title="Patent – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Patent">Patent – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Gottfried_Wilhelm_Leibniz" alt="Gottfried Wilhelm Leibniz – Wikipedia" title="Gottfried Wilhelm Leibniz – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Gottfried_Wilhelm_Leibniz">Gottfried Wilhelm Leibniz – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Woody_Allen" alt="Woody Allen – Wikipedia" title="Woody Allen – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Woody_Allen">Woody Allen – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Freie_Lizenz" alt="Freie Lizenz – Wikipedia" title="Freie Lizenz – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Freie_Lizenz">Freie Lizenz – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://en.wikipedia.org/wiki/Wicked_problem" alt="Wicked problem - Wikipedia" title="Wicked problem - Wikipedia" /> <a href="https://en.wikipedia.org/wiki/Wicked_problem">Wicked problem - Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Design_Thinking" alt="Design Thinking – Wikipedia" title="Design Thinking – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Design_Thinking">Design Thinking – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Kognitive_Verzerrung" alt="Kognitive Verzerrung – Wikipedia" title="Kognitive Verzerrung – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Kognitive_Verzerrung">Kognitive Verzerrung – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Mem" alt="Mem – Wikipedia" title="Mem – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Mem">Mem – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://imgflip.com/memegenerator/136488757/Deadline-horse" alt="Deadline horse Meme Generator - Imgflip" title="Deadline horse Meme Generator - Imgflip" /> <a href="https://imgflip.com/memegenerator/136488757/Deadline-horse">Deadline horse Meme Generator - Imgflip</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://knowyourmeme.com/memes/my-parents-are-dead-batman-slapping-robin" alt="My Parents Are Dead / Batman Slapping Robin | Know Your Meme" title="My Parents Are Dead / Batman Slapping Robin | Know Your Meme" /> <a href="https://knowyourmeme.com/memes/my-parents-are-dead-batman-slapping-robin">My Parents Are Dead / Batman Slapping Robin | Know Your Meme</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Melvin_Kranzberg" alt="Melvin Kranzberg – Wikipedia" title="Melvin Kranzberg – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Melvin_Kranzberg">Melvin Kranzberg – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Bruce_Schneier" alt="Bruce Schneier – Wikipedia" title="Bruce Schneier – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Bruce_Schneier">Bruce Schneier – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://en.wikipedia.org/wiki/Code_poetry" alt="Code poetry - Wikipedia" title="Code poetry - Wikipedia" /> <a href="https://en.wikipedia.org/wiki/Code_poetry">Code poetry - Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Hacker" alt="Hacker – Wikipedia" title="Hacker – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Hacker">Hacker – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://www.ideo.com/post/method-cards" alt="Method Cards" title="Method Cards" /> <a href="https://www.ideo.com/post/method-cards">Method Cards</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Start-up-Unternehmen" alt="Start-up-Unternehmen – Wikipedia" title="Start-up-Unternehmen – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Start-up-Unternehmen">Start-up-Unternehmen – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Beweisbare_Sicherheit" alt="Beweisbare Sicherheit – Wikipedia" title="Beweisbare Sicherheit – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Beweisbare_Sicherheit">Beweisbare Sicherheit – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://en.wikipedia.org/wiki/Internet_background_noise" alt="Internet background noise - Wikipedia" title="Internet background noise - Wikipedia" /> <a href="https://en.wikipedia.org/wiki/Internet_background_noise">Internet background noise - Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://forschergeist.de/podcast/fg016-open-science/" alt="FG016 Open Science | Forschergeist" title="FG016 Open Science | Forschergeist" /> <a href="https://forschergeist.de/podcast/fg016-open-science/">FG016 Open Science | Forschergeist</a></li>

    </ul>
    
    
    </div>
    
    
</div>



<hr style="clear: both;" />

    
<div class="podlove_cc_license">
<img src="https://forschergeist.de/wp-content/plugins/podlove-podcasting-plugin-for-wordpress/images/cc/10_1.png" alt="License" />
<p>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0">Creative Commons Attribution-ShareAlike 4.0 International License</a>
</p>
</div>��author��'Metaebene Personal Media - Tim Pritlove��id��@c3f432713d8d0b5195fd70acd912ff7e8a2fe88aa2b16447d27977653e814701��length��64346759��type��
audio/opus��href��chttps://forschergeist.de/podlove/file/2018/s/feed/c/opus/fg072-verantwortung-in-der-informatik.opus��	file_path�N�duration�M��chapters�]�(�
feedparser��FeedParserDict���)��(�start��00:00:00.000��title��Intro��start_parsed��datetime��	timedelta���K K K ��R�uh))��(�start��00:00:42.751��title��Begrüßung�h/h2K K*J�u ��R�uh))��(�start��00:01:29.581��title��Peter Purgathofer�h/h2K KYJ�� ��R�uh))��(�start��00:09:55.766��title��!Ethik und Moral in der Informatik�h/h2K MSJ0� ��R�uh))��(�start��00:19:22.390��title��Entscheidung und Verantwortung�h/h2K M�Jp� ��R�uh))��(�start��00:26:04.218��title��Denkweisen in der Informatik�h/h2K MJ�S ��R�uh))��(�start��00:37:26.608��title��Strukturierung des Kurses�h/h2K M�J G	 ��R�uh))��(�start��00:38:22.299��title��Pre-Scientific Thinking�h/h2K M�J�� ��R�uh))��(�start��00:43:54.001��title��Scientific Thinking�h/h2K MJ
M���R�uh))��(�start��00:48:09.791��title��Mathematical Thinking�h/h2K MIJ� ��R�uh))��(�start��00:54:11.171��title��Computational Thinking�h/h2K M�J�� ��R�uh))��(�start��01:01:46.949��title��Design Thinking�h/h2K MzJ{ ��R�uh))��(�start��01:11:44.567��title��Critical Thinking und Diversity�h/h2K M�Jئ ��R�uh))��(�start��01:22:05.020��title��Responsible Thinking�h/h2K M=M N��R�uh))��(�start��01:27:20.474��title��Creative Thinking�h/h2K MxJ�; ��R�uh))��(�start��01:31:20.975��title��Economical Thinking�h/h2K MhJ�� ��R�uh))��(�start��01:33:39.328��title��Criminal Thinking�h/h2K M�J@ ��R�uh))��(�start��01:37:46.757��title�� Denkweisen für die Wissenschaft�h/h2K M�J� ��R�uh))��(�start��01:39:53.808��title��Der Reifegrad der Informatik�h/h2K MiJ@T ��R�uh))��(�start��01:51:39.450��title��Ausklang�h/h2K M+J�� ��R�ueub.