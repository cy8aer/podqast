���6      �podcast.podpost��Podpost���)��}�(�isaudio���position�K �state�K �
percentage�K �favorite���podurl��-https://feeds.metaebene.me/forschergeist/opus��logo_url��r/home/nemo/.local/share/harbour-podqast/icons/dafb0dd2617b0b11b4d6ec8f8a4787972854eccfd1efdcd920708e0abe86775c.jpg��	logo_path�h�insert_date�N�	published�GA׳ �@  �title�� FG080 Medien und Meinungsbildung��link��Bhttps://forschergeist.de/podcast/fg080-medien-und-meinungsbildung/��	plainpart�X�  Doch, doch, es gab ein Internet vor Facebook, Twitter und YouTube. In den
2000er Jahren blühte die Blogosphäre auf. Statt nur passiv zu konsumieren
begannen Webnutzer mit eigenen Inhalten eine Öffentlichkeit zu finden –
argwöhnisch beobachtet von den klassischen Massenmedien, die Blogs auch mal
als „Klowände des Internets“ verächtlich machten. Doch die alten, klar
verteilten Rollen zwischen Journalisten und Lesern lösen sich seither immer
mehr auf. Die Deutungshoheit traditioneller Medien bröckelt. Soziale
Netzwerke, die seit rund zehn Jahren auch große Player im Werbemarkt sind,
stellen zudem die wirtschaftliche Grundlage der Verlage in Frage. Den Wandel
der digitalen Öffentlichkeit intensiv verfolgt hat Jan-Hinrik Schmidt, Senior
Researcher am Leibniz-Institut für Medienforschung / Hans-Bredow-Institut in
Hamburg. Die Digitalisierung bedeutet weit mehr als nur eine technische
Umstellung, denn die Grundlogik der neuen, datengetriebenen Plattformen geht
einher mit einer eigenen Mechanik für Identitäts-, Beziehungs- und
Informationsmanagement. Nachricht und Kommentar rücken in der Wahrnehmung
enger zusammen. Algorithmen belohnen Kommunikation, die zugespitzt ist, und
können an der Empörungsspirale drehen. Unterdessen hat es vernunftgeleitete
Argumentation schon ein bisschen schwerer, sich Gehör zu verschaffen, weil sie
naturgemäß nicht zur Erhöhung des Erregungslevels beitragen kann. Und dieser
Trend kann Folgen für das Miteinander in einer Demokratie haben. Für den
Soziologen Jan-Hinrik Schmidt steht fest: Die Regulierung der Plattformen wird
eine medienpolitische Schlüsselfrage in den 2020er-Jahren sein – findet doch
immer mehr Kommunikation in einer dezentralen Netzwerköffentlichkeit statt.
Und so steht im Zentrum von Schmidts Forschung künftig die Frage: Können die
sozialen Medien den gesellschaftlichen Zusammenhalt stärken oder bewirken sie
das Gegenteil?

��htmlpart�Xk'  <p class="episode_subtitle"><strong><em>Über das Aufkommen der Sozialen Medien, ihre Koexistenz mit klassischen Massenmedien und das heutige Gesamtgefüge der Meinungsbildung im Online-Zeitalter</em></strong></p>

            <p class="episode_meta">
                     
                    
</p>

            
<div style="float:right; margin-left: 10px;">
<table>
<colgroup>
    <col width="128px" />
        </colgroup>
<tr>
    <td style="vertical-align: top; width: 128px">
                            
<img alt="Jan-Hinrik Schmidt" title="Jan-Hinrik Schmidt" width="128" height="128" src="https://forschergeist.de/wp-content/cache/podlove/ff/ae4f2b0b95e407d4a82acd39825bf2/jan-hinrik-schmidt_128x128.jpg" srcset="https://forschergeist.de/wp-content/cache/podlove/ff/ae4f2b0b95e407d4a82acd39825bf2/jan-hinrik-schmidt_128x128.jpg 1x, https://forschergeist.de/wp-content/cache/podlove/ff/ae4f2b0b95e407d4a82acd39825bf2/jan-hinrik-schmidt_256x256.jpg 2x, https://forschergeist.de/wp-content/cache/podlove/ff/ae4f2b0b95e407d4a82acd39825bf2/jan-hinrik-schmidt_384x384.jpg 3x"/>

        <br/>
<strong>Jan-Hinrik Schmidt</strong>
                                                            <br/>                <a target="_blank" title="Xing" href="https://www.xing.com/profile/JanHinrik_Schmidt/cv">
                                    
<img class="podlove-contributor-button" alt="Xing" width="32" src="https://forschergeist.de/podlove/image/68747470733a2f2f666f72736368657267656973742e64652f77702d636f6e74656e742f706c7567696e732f706f646c6f76652d706f6463617374696e672d706c7567696e2d666f722d776f726470726573732f6c69622f6d6f64756c65732f736f6369616c2f696d616765732f69636f6e732f78696e672e706e67/32/0/0/xing"/>

                   </a>
                                                            <a target="_blank" title="Twitter" href="https://twitter.com/JanSchmidt">
                                    
<img class="podlove-contributor-button" alt="Twitter" width="32" height="32" src="https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_32x32.png" srcset="https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_32x32.png 1x, https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_64x64.png 2x, https://forschergeist.de/wp-content/cache/podlove/00/8de710527285c87999ce25ee01b9ed/twitter_96x96.png 3x"/>

                   </a>
                                                            <a target="_blank" title="Website" href="https://www.hans-bredow-institut.de/de/mitarbeiter/jan-hinrik-schmidt">
                                    
<img class="podlove-contributor-button" alt="Website" width="32" height="32" src="https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_32x32.png" srcset="https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_32x32.png 1x, https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_64x64.png 2x, https://forschergeist.de/wp-content/cache/podlove/86/c2058124a7041740cde213151039aa/website_96x96.png 3x"/>

                   </a>
                            </td>
            </tr>
    </table> 
</div>

            <p class="episode-summary">Doch, doch, es gab ein Internet vor Facebook, Twitter und YouTube. In den 2000er Jahren blühte die Blogosphäre auf. Statt nur passiv zu konsumieren begannen Webnutzer mit eigenen Inhalten eine Öffentlichkeit zu finden – argwöhnisch beobachtet von den klassischen Massenmedien, die Blogs auch mal als „Klowände des Internets“ verächtlich machten. Doch die alten, klar verteilten Rollen zwischen Journalisten und Lesern lösen sich seither immer mehr auf. Die Deutungshoheit traditioneller Medien bröckelt. Soziale Netzwerke, die seit rund zehn Jahren auch große Player im Werbemarkt sind, stellen zudem die wirtschaftliche Grundlage der Verlage in Frage.<br />
<br />
Den Wandel der digitalen Öffentlichkeit intensiv verfolgt hat Jan-Hinrik Schmidt, Senior Researcher am Leibniz-Institut für Medienforschung / Hans-Bredow-Institut in Hamburg. Die Digitalisierung bedeutet weit mehr als nur eine technische Umstellung, denn die Grundlogik der neuen, datengetriebenen Plattformen geht einher mit einer eigenen Mechanik für Identitäts-, Beziehungs- und Informationsmanagement. Nachricht und Kommentar rücken in der Wahrnehmung enger zusammen. Algorithmen belohnen Kommunikation, die zugespitzt ist, und können an der Empörungsspirale drehen. Unterdessen hat es vernunftgeleitete Argumentation schon ein bisschen schwerer, sich Gehör zu verschaffen, weil sie naturgemäß nicht zur Erhöhung des Erregungslevels beitragen kann. Und dieser Trend kann Folgen für das Miteinander in einer Demokratie haben.<br />
<br />
Für den Soziologen Jan-Hinrik Schmidt steht fest: Die Regulierung der Plattformen wird eine medienpolitische Schlüsselfrage in den 2020er-Jahren sein – findet doch immer mehr Kommunikation in einer dezentralen Netzwerköffentlichkeit statt. Und so steht im Zentrum von Schmidts Forschung künftig die Frage: Können die sozialen Medien den gesellschaftlichen Zusammenhalt stärken oder bewirken sie das Gegenteil?</p>





 <!-- is not feed -->


<hr style="clear: both;" />


<h2>Shownotes</h2>

<div class='osfx-shownote-block'>

    <div class="links">

    </div>

    

<div class='glossary'>
            <h3>Glossar</h3>
    <ul>
    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Hans-Bredow-Institut" alt="Hans-Bredow-Institut – Wikipedia" title="Hans-Bredow-Institut – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Hans-Bredow-Institut">Hans-Bredow-Institut – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Blog" alt="Blog – Wikipedia" title="Blog – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Blog">Blog – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Re:publica" alt="re:publica – Wikipedia" title="re:publica – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Re:publica">re:publica – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Blogosph%C3%A4re" alt="Blogosphäre – Wikipedia" title="Blogosphäre – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Blogosph%C3%A4re">Blogosphäre – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Web_2.0" alt="Web 2.0 – Wikipedia" title="Web 2.0 – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Web_2.0">Web 2.0 – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Gatekeeper_(Nachrichtenforschung)" alt="Gatekeeper (Nachrichtenforschung) – Wikipedia" title="Gatekeeper (Nachrichtenforschung) – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Gatekeeper_(Nachrichtenforschung)">Gatekeeper (Nachrichtenforschung) – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Nachrichtenwert" alt="Nachrichtenwert – Wikipedia" title="Nachrichtenwert – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Nachrichtenwert">Nachrichtenwert – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/J%C3%BCrgen_Habermas" alt="Jürgen Habermas – Wikipedia" title="Jürgen Habermas – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/J%C3%BCrgen_Habermas">Jürgen Habermas – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Verschw%C3%B6rungstheorie" alt="Verschwörungstheorie – Wikipedia" title="Verschwörungstheorie – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Verschw%C3%B6rungstheorie">Verschwörungstheorie – Wikipedia</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://www.ichbinhier.eu/" alt="ichbinhier" title="ichbinhier" /> <a href="https://www.ichbinhier.eu/">ichbinhier</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://www.bmbf.de/de/institut-fuer-gesellschaftlichen-zusammenhalt-startet-7044.html" alt="„Institut für gesellschaftlichen Zusammenhalt“ startet - BMBF" title="„Institut für gesellschaftlichen Zusammenhalt“ startet - BMBF" /> <a href="https://www.bmbf.de/de/institut-fuer-gesellschaftlichen-zusammenhalt-startet-7044.html">„Institut für gesellschaftlichen Zusammenhalt“ startet - BMBF</a></li>


    

<li><span style="display: inline-block" class="glossary"><img src="https://www.google.com/s2/favicons?domain=https://de.wikipedia.org/wiki/Datenjournalismus" alt="Datenjournalismus – Wikipedia" title="Datenjournalismus – Wikipedia" /> <a href="https://de.wikipedia.org/wiki/Datenjournalismus">Datenjournalismus – Wikipedia</a></li>

    </ul>
    
    
    </div>
    
    
</div>



<hr style="clear: both;" />

    
<div class="podlove_cc_license">
<img src="https://forschergeist.de/wp-content/plugins/podlove-podcasting-plugin-for-wordpress/images/cc/10_1.png" alt="License" />
<p>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0">Creative Commons Attribution-ShareAlike 4.0 International License</a>
</p>
</div>��author��'Metaebene Personal Media - Tim Pritlove��id��@3fe572963255e2af0325e9ea727a991d88301a7702a493207395241f081204b1��length��48705241��type��
audio/opus��href��^https://forschergeist.de/podlove/file/2228/s/feed/c/opus/fg080-medien-und-meinungsbildung.opus��	file_path�N�duration�M��chapters�]�(�
feedparser��FeedParserDict���)��(�start��00:00:00.000��title��Intro��start_parsed��datetime��	timedelta���K K K ��R�uh))��(�start��00:00:42.995��title��Begrüßung�h/h2K K*J�. ��R�uh))��(�start��00:01:40.144��title��Vorstellung�h/h2K KdJ�2 ��R�uh))��(�start��00:08:40.665��title��Die Blogosphäre�h/h2K MJ�%
 ��R�uh))��(�start��00:13:41.939��title��Neue Öffentlichkeiten�h/h2K M5J�S ��R�uh))��(�start��00:20:23.003��title��Blogs und Journalismus�h/h2K M�M���R�uh))��(�start��00:27:13.230��title��Plattformen als Intermediäre�h/h2K MaJp� ��R�uh))��(�start��00:32:21.566��title��Netzwerk-Effekt�h/h2K M�J� ��R�uh))��(�start��00:36:11.662��title��Rolle der Massenmedien�h/h2K M{J�
 ��R�uh))��(�start��00:43:30.115��title��Medien in der Corona-Krise�h/h2K M2
J8� ��R�uh))��(�start��00:52:18.638��title��Meinungsbildung in den Netzen�h/h2K MBJ0�	 ��R�uh))��(�start��01:08:04.045��title��Regulierung sozialer Medien�h/h2K M�Mȯ��R�uh))��(�start��01:16:57.433��title��(Institut Gesellschaftlicher Zusammenhalt�h/h2K M	Jh� ��R�uh))��(�start��01:24:50.741��title��Ausklang�h/h2K M�J�N ��R�ueub.