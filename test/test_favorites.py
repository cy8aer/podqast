from httpretty import HTTPretty, httprettified

from podcast.archive import ArchiveFactory
from podcast.podpost import PodpostFactory
from podcast.util import ilen
from . import xml_headers, read_testdata


@httprettified(allow_net_connect=False)
def test_queue_favorites():
    """
    Test listing of podposts
    """
    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)

    from podcast import favorite
    from podcast.podcast import Podcast

    url = 'https://freakshow.fm/feed/opus/'
    podcast, episodes = Podcast.create_from_url(url)
    fav_posts = [PodpostFactory().get_podpost(podcast.entry_ids_old_to_new[i]) for i in range(0, 10, 2)]
    assert len(fav_posts) == 5
    archive = ArchiveFactory().get_archive()
    assert ilen(archive.get_podposts()) == 0
    for post in fav_posts:
        post.favorite = True
        PodpostFactory().persist(post)
        archive.insert(post.id)
    assert ilen(favorite.get_favorites()) == 5
