"""
Store: a file store for pickling elements
"""
import logging
import os
import pickle
import hashlib
from pathlib import Path
import sys
from typing import Iterable

from podcast.util import ilen

sys.path.append("../")


class DeprecatedStore:
    def __init__(self, directory):
        """
        initialize the Store.
        directory: File directory the date should be stored to
        """

        self.directory = directory

    def _hashme(self, name):
        """
        create a hash from name
        """

        return hashlib.sha256(name.encode()).hexdigest() + ".pickle"

    def store(self, index, element):
        """
        store the element in self.directory
        index: store index name
        element: the element to be pickeled
        """

        fname = self._hashme(index)
        with open(os.path.join(self.directory, fname), "wb") as in_handle:
            pickle.dump(element, in_handle)

    def delete(self, index):
        """
        delete an element file
        index: index of the file
        """

        fname = self._hashme(index)
        path = Path(self.directory + "/" + fname)

        if path.exists():
            os.remove(self.directory + "/" + fname)

    def get(self, index):
        """
        get the element from file
        """

        fname = self._hashme(index)
        path = Path(self.directory + "/" + fname)
        if path.exists():
            try:
                with open(self.directory + "/" + fname, "rb") as out_handle:
                    return pickle.load(out_handle)
            except:
                return None
        else:
            return None

    def count(self):
        return ilen(1 for filename in os.listdir(self.directory) if filename.endswith(".pickle"))

    def iter(self, key_filter: Iterable[str]):
        """
        iterates over all elements in the store
        @param key_filter:  list of keys that should be in the resulting dict
        @yields: each pickle as dict in the store that has the given keys
        """
        key_filter_set = set(key_filter)
        for file in os.listdir(self.directory):
            filename = os.fsdecode(file)
            try:
                if filename.endswith(".pickle"):
                    with open(self.directory + "/" + filename, "rb") as out_handle:
                        loaded_object = pickle.load(out_handle)
                        if loaded_object:
                            loaded_dict  = loaded_object.__dict__
                            if key_filter_set.issubset(loaded_dict.keys()):
                                yield loaded_dict, filename[0:-len(".pickle")]

            except:
                logging.exception("could not load pickle")

    def exists(self, index):
        """
        check if an index exists as persitent file
        index: The file index
        """

        fname = self._hashme(index)
        path = Path(self.directory + "/" + fname)
        return path.exists()
