"""
Podpost: a single podcast post
"""
import datetime
import sys
from functools import reduce
from math import floor
from urllib.error import URLError

from podcast.persistent_log import persist_log, LogType

sys.path.append("../")
from podcast.podcast import Podcast
from podcast import util
from podcast.constants import Constants, BaseModel
from peewee import AutoField, TextField, IntegerField, DateTimeField, BooleanField, CharField, \
    FloatField, DoesNotExist, ForeignKeyField, ModelSelect

import hashlib
import os
from urllib.parse import urlparse
import html2text
from email.utils import mktime_tz, parsedate_tz
from calendar import timegm
from typing import List, Iterator, Optional, Dict, Union

from podcast.factory import BaseFactory
import pyotherside
import time
import re
import logging

logger = logging.getLogger(__name__)

PLAY = 1
PAUSE = 2
STOP = 0


class Podpost(BaseModel):
    """
    One podcast post/episode.
    Most fields are persisted in the database
    """
    guid: Union[str,TextField]  = CharField(index=True)
    id: Union[int,AutoField] = AutoField(primary_key=True)  #
    author: Union[str,TextField]  = CharField(default="")
    duration: Union[int,IntegerField] = IntegerField(null=True, help_text="in ms")
    favorite: Union[bool,BooleanField] = BooleanField(default=False)
    file_path: Union[str,TextField]  = TextField(null=True)
    # podcast file url
    href: Union[str,TextField]  = TextField(default="", index=True)
    htmlpart: Union[str,TextField]  = TextField(null=True)
    # when we entered this post into our db
    insert_date = FloatField(default=lambda: datetime.datetime.now().timestamp())
    # if the post comes from the external folder
    isaudio: bool = BooleanField(default=False, help_text="Is episode from external folder source")
    # filesize
    length: Union[int,IntegerField] = IntegerField(default=0)
    listened: bool = BooleanField(default=False, null=False, help_text="Has been played already")
    link: Union[str,TextField]  = TextField(default="")
    logo_path: Union[str,TextField] = TextField(null=True)
    logo_url: Union[str,TextField] = TextField(null=True)
    # download percentage
    percentage: float = FloatField(default=0)
    plainpart: TextField = TextField(default="")
    position: Union[int,IntegerField] = IntegerField(default=0, help_text="in ms")
    podcast: Union[Podcast,ForeignKeyField] = ForeignKeyField(Podcast, null=True, backref='episodes', lazy_load=True, on_delete='CASCADE')
    # when the post was published according to feed
    published = DateTimeField()
    # play_state
    state: Union[int,IntegerField] = IntegerField(default=STOP)
    title: Union[str,TextField] = TextField()
    # mimetype
    type: Union[str,TextField] = TextField(null=True)

    def __init__(self, *args, **kwargs):
        """
        Thank you podcast providers for not using any standards. This init is
        the worst spaghetti I ever wrote...

        entry: the entry took from feedparser
        podcast: podcast url
        """
        super().__init__(*args, **kwargs)

    @classmethod
    def from_feed_entry(cls, podcast, entry, podurl) -> 'Podpost':
        """
        Creates a podpost from a feed entry
        @rtype: Podpost
        """
        assert podurl
        post = cls()
        post.podcast = podcast
        if 'image' in entry.keys():
            image_url: str = entry["image"].href
            if image_url and isinstance(image_url,str) and image_url.startswith("http") and image_url != podcast.logo_url:
                post.logo_url = image_url

        if 'published_parsed' in entry.keys():
            post.published = timegm(entry['published_parsed'])
        else:
            if "published" in entry.keys():
                logger.debug("no published_parsed")
                if type(entry["published"]) == float:
                    post.published = entry["published"]
                else:
                    try:
                        post.published = mktime_tz(parsedate_tz(entry["published"]))
                    except:
                        logger.warning("Could not parse published % of entry %s", entry["published"], post.title)
                        post.published = 0
            else:
                post.published = 0
        post.title = entry["title"]
        if "link" in entry:
            post.link = entry["link"]
        else:
            pass
        if "content" in entry.keys():
            for cont in entry["content"]:
                if cont.type == "text/html":
                    post.htmlpart = cont.value
                if cont.type == "text/plain":
                    post.plainpart = cont.value
        if not post.plainpart:
            if "summary" in entry.keys():
                post.plainpart = entry["summary"]
            else:
                post.plainpart = ""
        if not post.htmlpart:
            if "summary_detail" in entry.keys():
                post.htmlpart = entry["summary_detail"].value
            else:
                post.htmlpart = ""
        h = html2text.HTML2Text()
        h.ignore_links = True
        h.ignore_images = True
        post.plainpart = h.handle(post.plainpart)
        if "author" in entry.keys():
            post.author = entry["author"]
        if "id" in entry:
            if entry["id"] != "":
                post.guid = entry["id"]
        else:
            post.guid = hashlib.sha256(entry["summary"].encode()).hexdigest()
        if len(entry["enclosures"]) == 0:
            logger.warning("post %s has no enclosures", post.title)
        for e in entry["enclosures"]:
            if "href" in e:
                if e.type[:5] == "audio":
                    if "length" in e.keys():
                        post.length = e.length
                    else:
                        post.length = 0
                    post.type = e.type
                    post.href = e.href
                    if "guid" in e.keys():
                        post.guid = e.guid
                elif e.href[-3:] == "mp3":
                    if "length" in e.keys():
                        post.length = e.length
                    else:
                        post.length = 0
                    post.type = "audio/mp3"
                    post.href = e.href
        if "itunes_duration" in entry:
            post.duration = util.tx_to_s(entry["itunes_duration"]) * 1000
        else:
            post.duration = 0
        post.chapters = list(PodpostChapter.from_feed_entry(post, entry))
        if not post.href:
            logger.warning("Did not set href for post %s", post.title)
        return post

    @classmethod
    def from_audio_file(cls, afile) -> 'Podpost':
        """
        initialize the class from audio file data
        """

        iconpath = util.down_audio_icon(afile, Constants().iconpath)
        meta = util.get_audio_meta(afile)
        tech = util.get_audio_tech(afile)
        desc = meta["title"] + " - " + meta["artist"] + " - " + meta["genre"]
        theid = hashlib.sha256(afile.encode()).hexdigest()
        filesize = os.path.getsize(afile)

        post = cls()
        post.title = meta["title"]
        post.plainpart = post.htmlpart = desc
        post.author = meta["artist"]
        post.logo_url = post.logo_path = iconpath
        post.file_path = afile
        post.href = afile
        post.duration = int(tech["length"])
        post.guid = theid
        post.length = int(filesize)
        post.published = 0
        post.type = "audio/" + tech["filetype"]
        post.isaudio = True

        return post

    def get_data(self) -> Dict[str,object]:
        """
        return a dict of main data
        """

        from podcast.queue import QueueFactory

        inqueue = QueueFactory().get_queue().is_in_queue(self)

        section = util.format_full_date(self.published)
        date = self.published
        asection = util.format_full_date(self.insert_date)

        loaded = False
        if self.isaudio:
            try:
                loaded = os.path.exists(self.href)
            except:
                loaded = False

        if len(self.chapters) > 0:
            haschapters = True
        else:
            haschapters = False
        return {
            "id": self.id,
            "title": self.title,
            "podcast_url": self.podcast.url if not self.isaudio else "",
            "audio_url": self.href,
            "podcast_logo": self.podcast.logo() if not self.isaudio else "",
            "episode_logo": self.get_episode_logo_path_or_url(),
            "link": self.link,
            "description": self.plainpart,
            "detail": self.htmlpart,
            "position": self.position,
            "date": date,
            "section": section,
            "asection": asection,
            "length": self.length,
            "duration": self.duration,
            "dlperc": self.percentage,
            "favorite": self.favorite,
            "inqueue": inqueue,
            "is_external_audio": self.isaudio,
            "loaded": loaded,
            "haschapters": haschapters,
            "listened": self.listened or self.position > 0 and (
                    self.duration - self.position < Constants().markListenedBeforeEndThreshold * 1000),
            "podcastTitle": self.podcast.title if not self.isaudio else ""
        }

    def get_episode_logo_path_or_url(self):
        if self.logo_path != None:
            image = self.logo_path
        else:
            image = self.logo_url
        return image

    def download(self):
        yield from self.__download_audio()
        try:
            self.__download_icon()
        except:
            logger.exception("exception during downloading logo")
        PodpostFactory().persist(self)

    def __download_audio(self) -> Iterator[int]:
        """
        Download the audio stuff
        """

        logger.info("Downloading audio for %s", self.title)

        if self.file_path and self.percentage == 100:
            if os.path.exists(self.file_path):
                return

        afilepath = Constants().audiofilepath
        filename = os.path.basename(urlparse(self.href).path)
        rdot = filename.rfind(".")
        ext = filename[rdot + 1:]

        name = hashlib.sha256(self.href.encode()).hexdigest() + "." + ext
        namepart = name + ".part"

        file_path = os.path.join(afilepath, name)
        file_path_part = os.path.join(afilepath, namepart)
        # TODO this should probably check if the download is still running
        if os.path.isfile(file_path_part):
            if time.time() - os.path.getmtime(file_path_part) < 2 * 60:
                logger.warning(
                    "audio download: part file younger than 2 min"
                )
                return
        # todo do this when setting href
        href = self.remove_trackers()
        try:
            for perc in util.dl_from_url_progress(href, file_path_part):
                self.percentage = perc
                yield perc
            self.percentage = 100

            os.rename(file_path_part, file_path)
        except URLError:
            logger.exception("Download failed")
            self.delete_file()
            file_path = None
            self.percentage = 0
            persist_log(LogType.NetworkError, what="episode download", title=self.title, url=self.href)
        except BaseException as e:
            logger.exception("renaming the downloaded file failed")
            file_path = None
            self.percentage = 0
            persist_log(LogType.Exception, what="episode download", title=self.title, exception=e)

        self.file_path = file_path

    def __download_icon(self):
        """
        Download icon
        """

        if self.logo_path:
            if os.path.exists(self.logo_path):
                return

        iconpath = Constants().iconpath
        rdot = self.logo_url.rfind(".")
        ext = self.logo_url[rdot + 1:]
        if ext == "jpg" or ext == "jpeg" or ext == "png" or ext == "gif":
            name = (
                    hashlib.sha256(self.logo_url.encode()).hexdigest() + "." + ext
            )

            logo_path = os.path.join(iconpath, name)
            try:
                util.dl_from_url(self.logo_url, logo_path)
            except:
                logo_path = ""

            self.logo_path = logo_path
        else:
            self.logo_path = ""

    def play(self) -> Dict[str,object]:
        """
        Start playing. Returns the audio file and it's actual position
        """

        if self.file_path and self.percentage == 100:
            url = self.file_path
        else:
            url = self.remove_trackers()

        self.state = PLAY

        if self.isaudio:
            if not os.path.exists(url):
                url = None

        return {"url": url, "position": self.position}

    def remove_trackers(self) -> str:
        href = self.href
        if re.search("dts.podtrac.com", href):
            url = re.sub("https://.*dts.podtrac.com/redirect.mp3/", "https://", href)
            url = re.sub("http://.*dts.podtrac.com/redirect.mp3/", "http://", url)
        elif re.search("www.podtrac.com", href):
            url = re.sub("https://.*www.podtrac.com/pts/redirect.mp3/", "https://", href)
            url = re.sub("http://.*www.podtrac.com/pts/redirect.mp3/", "http://", url)
        elif re.search("chtbl.com", href):
            url = re.sub("https?://.*chtbl.com/track/\w*/", "https://", href)
        else:
            url = href
        return url

    def set_position(self, position):
        """
        set position
        """

        self.position = position
        if self.position > 0 and (
                self.duration - self.position < Constants().markListenedBeforeEndThreshold * 1000):
            self.listened = True
        PodpostFactory().persist(self)
        
    def reset_position_if_almost_finished(self):
        if self.duration == 0 or (self.duration - self.get_position) / 1000 < 60:
            logger.debug("duration near position")
            self.position = 0
            PodpostFactory().persist(self)

    @property
    def get_position(self) -> int:
        """
        Return position in seconds
        """

        return self.position

    def pause(self, position):
        """
        Pause audio. Save the actual state
        """

        if position >= 0:
            self.position = position
        self.state = PAUSE
        PodpostFactory().persist(self)

    def stop(self, position):
        """
        Stop the play and save the position
        """

        if position >= 0:
            self.position = position
        self.state = STOP
        PodpostFactory().persist(self)

    @property
    def get_play_state(self):
        """
        get state of play. Will show the active element
        """

        return self.state

    @property
    def get_icon(self):
        """
        get url of icon
        """

        return self.logo_url

    def delete_file(self):
        """
        Delete downloaded File
        """

        if self.favorite:
            return

        if self.isaudio:
            return

        if self.file_path:
            util.delete_file(self.file_path)
            self.percentage = 0
            self.file_path = None
        if self.logo_path:
            util.delete_file(self.logo_path)
            self.logo_path = None
        PodpostFactory().persist(self)

    def toggle_fav(self, do_download=False):
        """
        toggle favorite flag (and create one if it not exists)
        return: bool if favorite
        """
        from podcast.queue import QueueFactory

        self.favorite = not self.favorite

        def get_topath():
            rdot = self.file_path.rfind(".")
            ext = self.file_path[rdot + 1:]
            tn = (
                str(self.title.encode("ascii", "ignore"))
                    .strip()
                    .replace(" ", "_")[1:]
            )
            tnb = re.sub(r"(?u)[^-\w.]", "", tn) + "." + ext
            tnb = "".join(x for x in tnb if x.isalnum() or x in "._-()")
            topath = os.path.join(Constants().favorites_home, tnb)

            pyotherside.send("topath: " + topath)
            return topath

        if self.favorite and do_download:
            logger.info("need to download file")
            for perc in self.download():
                pyotherside.send("downloading", self.id, perc)

            if self.file_path:
                util.make_symlink(self.file_path, get_topath())
        else:
            if self.file_path:
                queue = QueueFactory().get_queue()
                util.delete_file(get_topath())
                if self.id not in queue.get_podposts_ids():
                    self.delete_file()

        PodpostFactory().persist(self)
        return self.favorite

    @property
    def __ordered_chapters_query(self) -> ModelSelect:
        return self.chapters.select().order_by(PodpostChapter.start_millis)

    def get_chapters(self) -> List[Dict]:
        """
        Returns a list of chapters. Adds selected flags if not already present.
        """
        return [c.get_data() for c in self.__ordered_chapters_query]

    def toggle_chapter(self, chapterid):
        """
        Toggles the selected flag for a given chapter.
        chapterid: chapter number
        """

        chapter: PodpostChapter = list(self.__ordered_chapters_query.offset(chapterid).limit(1))[0]
        chapter.selected = not chapter.selected
        chapter.save()

    @property
    def get_fav(self):
        """
        Return Favorite state
        """

        return self.favorite

    def set_duration(self, duration):
        self.duration = duration
        PodpostFactory().persist(self)

    def update_position(self, position):
        self.position = position
        PodpostFactory().persist(self)


class PodpostChapter(BaseModel):
    podpost: Podpost = ForeignKeyField(Podpost, backref="chapters", lazy_load=False, on_delete='CASCADE')
    start_millis: datetime.time = IntegerField()
    title: str = TextField(default="")
    selected: bool = BooleanField(default=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_data(self):
        return {
            'title': self.title,
            'start_millis': self.start_millis,
            'selected': self.selected
        }

    @classmethod
    def from_feed_entry(cls, podpost: Podpost, entry) -> Iterator:
        if "psc_chapters" in entry:
            for chapter in entry.psc_chapters.chapters:
                yield cls(podpost=podpost, title=chapter.title if 'title' in chapter else '',
                          start_millis=chapter.start_parsed / datetime.timedelta(milliseconds=1))

    @classmethod
    def get_bulk_size(cls):
        max_sql_vars = 999
        chapter_fields = 5
        return floor(max_sql_vars / chapter_fields)


class PodpostFactory(BaseFactory):

    def __init__(self):
        super().__init__()

    def get_podpost(self, index) -> Optional[Podpost]:
        """
        Get a podpost
        """
        try:
            return Podpost.get_by_id(index)
        except DoesNotExist:
            return None

    def exists(self, index):
        return Podpost.select(Podpost.id == index).exists()

    def delete_podpost(self, index):
        Podpost.delete_by_id(index)
        PodpostChapter.delete().where(PodpostChapter.podpost == index).execute()

    def persist(self, post: Podpost, store_chapters=False):
        if type(post) != Podpost:
            raise ValueError("Can only persist podposts, not %s", type(post))
        post.save()
        if store_chapters:
            PodpostChapter.bulk_create(post.chapters,
                                       batch_size=PodpostChapter.get_bulk_size())

    def create(self, post: Podpost):
        post.create()

    def get_podcast_posts(self, podcast, limit=-1) -> Iterator[Podpost]:
        """
        @type podcast: Podcast
        """
        from podcast.podcast import Podcast
        if (type(podcast) != Podcast):
            raise ValueError("supplied argument must be a podcast")
        query: ModelSelect = Podpost.select().where(Podpost.podcast == podcast).order_by(Podpost.published.desc())
        if limit > 0:
            query = query.limit(limit)
        for post in query.iterator():
            yield post

    def persist_bulk(self, new_posts: List[Podpost]):
        if len(new_posts) == 0:
            return
        # bulk_create does not update the id unfortunatley, thus we can't  use it for posts, because we need to add chapters
        for post in new_posts:
            post.save()

        # flatmap chapters for bulk create
        PodpostChapter.bulk_create(reduce(list.__add__, (post.chapters for post in new_posts)),
                                   batch_size=PodpostChapter.get_bulk_size())

    def delete_all_from_podcast(self, podcast):
        """
        @type podcast: Podcast
        """
        from podcast.podcast import Podcast
        if (type(podcast) != Podcast):
            raise ValueError("supplied argument must be a podcast")
        for post_id in podcast.entry_ids_old_to_new:
            self.delete_podpost(post_id)
