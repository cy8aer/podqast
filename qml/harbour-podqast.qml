import QtQuick 2.0
import QtMultimedia 5.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.Notifications 1.0
import Nemo.DBus 2.0
import io.thp.pyotherside 1.4
import Amber.Mpris 1.0

import "pages"
import "components"

ApplicationWindow {
    id: podqast

    property bool doWizzard: doWizzardConf.value

    property bool isInitialized: false

    // postponed until migrationhandler answers
    initialPage: Page {
        BusyIndicator {
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
            running: true
        }
    }

    MigrationHandler {
        id: migrationhandler
        onMigrationNeeded: if (!is_needed) {
                               podqast.isInitialized = true
                               var pagename
                               if (podqast.doWizzard === true)
                                   pagename = "pages/Wizzard1.qml"
                               else
                                   pagename = "pages/MainCarousel.qml"
                               console.log("Opening " + pagename + " as first page")
                               pageStack.replace(Qt.resolvedUrl(pagename))
                           } else {
                               console.log("Opening migrationpage")
                               pageStack.replace(Qt.resolvedUrl(
                                                     "pages/DataMigration.qml"))
                           }
    }

    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
    property bool playeropen: false
    property bool playparamdok: false
    property bool _showUpdatesNotification: true
    property alias wifiConnected: connmanWifi.wifiConnected
    property int aktchapter
    property string hfilter: "home"
    property string ffilter: "home"
    property string ifilter: "home"
    property bool needexternal: true
    property bool allowFavVal: doFavDownConf.value
    property bool allowExtVal: allowExtConf.value
    property int extMoveToVal: extMoveToConf.value
    property int markListenedEndThreshold: markListenedEndThresholdConf.value
    property string musicHomeVal: StandardPaths.music
    property string ainfolabel: ""
    property int sleep: sleepTimerConf.value
    property bool dosleep: false

    Timer {
        id: minuteman
        interval: 10 * 1000
        running: playerHandler.isPlaying
        repeat: true
        onTriggered: {
            console.log("Minuteman")
            playerHandler.interval_task()
        }
    }

    Timer {
        id: sleeptimer
        interval: sleep * 60 * 1000
        running: dosleep
        repeat: false
        onTriggered: {
            playerHandler.pause()
            dosleep = false
            sleep = sleepTimerConf.value
        }
        onRunningChanged: {
            console.log("sleeptimer running: " + running)
            console.log("sleeptimer length" + interval / 60 / 1000)
        }
    }

    Timer {
        id: refreshtimer
        interval: refreshTimeConf.value * 1000
        running: podqast.isInitialized && refreshTimeConf.value !== 0
                 && !migrationhandler.running
        repeat: true
        onTriggered: {
            console.log("automatic refresh")
            feedparserhandler.refreshPodcasts()
        }
    }

    Timer {
        id: chapterSkipperTimer
        interval: 1 * 1000
        running: podqast.isInitialized && playerHandler.isPlaying
                 && playerHandler.chapters.length > 0
        repeat: true
        onTriggered: {
            playerHandler.getaktchapter()
            if (playerHandler.chapters[playerHandler.aktchapter].selected === false) {
                playerHandler.nextselectedchapter()
            }
        }
    }

    Timer {
        id: downloadtimer
        interval: 2 * 60 * 1000
        running: podqast.isInitialized && doDownloadConf.value
                 && (wifiConnected || doMobileDownConf.value)
                 && !migrationhandler.running
        repeat: true
        onTriggered: {
            console.log("automatic new tries of download")
            queuehandler.downloadAudioAll()
        }
    }

    Timer {
        id: external_notify_timer
        interval: 5000
        running: podqast.isInitialized
        repeat: false
        onTriggered: {
            console.log("starting inotify thread")
            if (needexternal) {
                externalhandler.checkNew()
                externalhandler.waitNew()
                needexternal = false
            }
        }
    }

    ConfigurationValue {
        id: saveOnSDCardConf
        key: "/apps/ControlPanel/Podqast/saveOnSDCard"
        defaultValue: false
    }

    ConfigurationValue {
        id: dataTrackableConf
        key: "/apps/ControlPanel/podqast/dataTrackable"
        defaultValue: false
    }

    ConfigurationValue {
        id: useGpodderConf
        key: "/apps/ControlPanel/podqast/useGpodder"
        defaultValue: true
    }

    ConfigurationValue {
        id: doDownloadConf
        key: "/apps/ControlPanel/podqast/doDownload"
        defaultValue: true
    }

    ConfigurationValue {
        id: doMobileDownConf
        key: "/apps/ControlPanel/podqast/doMobileDown"
        defaultValue: false
    }

    ConfigurationValue {
        id: doFavDownConf
        key: "/apps/ControlPanel/podqast/doFavDown"
        defaultValue: false
    }

    ConfigurationValue {
        id: moveToConf
        key: "/apps/ControlPanel/podqast/moveTo"
        defaultValue: 0
    }

    ConfigurationValue {
        id: allowExtConf
        key: "/apps/ControlPanel/podqast/allowExt"
        defaultValue: false
    }

    ConfigurationValue {
        id: extMoveToConf
        key: "/apps/ControlPanel/podqast/extMoveTo"
        defaultValue: 0
    }

    ConfigurationValue {
        id: lastRefreshed
        key: "/apps/ControlPanel/podqast/lastRefreshed"
        defaultValue: 0
    }

    ConfigurationValue {
        id: refreshTimeConf
        key: "/apps/ControlPanel/podqast/refreshTime"
        defaultValue: 1 * 60 * 60
    }

    ConfigurationValue {
        id: doWizzardConf
        key: "/apps/ControlPanel/podqast/doWizzard"
        defaultValue: true
    }

    ConfigurationValue {
        id: autoLimitConf
        key: "/apps/ControlPanel/podqast/autoLimit"
        defaultValue: 2
    }

    ConfigurationValue {
        id: downLimitConf
        key: "/apps/ControlPanel/podqast/downLimit"
        defaultValue: 0
    }

    ConfigurationValue {
        id: globalPlayrateConf
        key: "/apps/ControlPanel/podqast/globalPlayrate"
        defaultValue: 1.0
    }

    ConfigurationValue {
        id: experimentalConf
        key: "/apps/ControlPanel/podqast/experimentalFlag"
        defaultValue: false
    }

    ConfigurationValue {
        id: useBluetoothConf
        key: "/apps/ControlPanel/podqast/bluetoothFlag"
        defaultValue: true
    }

    ConfigurationValue {
        id: sleepTimerConf
        key: "/apps/ControlPanel/podqast/sleepTimer"
        defaultValue: 15
    }

    ConfigurationValue {
        id: markListenedEndThresholdConf
        key: "/apps/ControlPanel/podqast/markListenedEndThreshold"
        defaultValue: 60
    }

    ConfigurationValue {
        id: autoPlayNextInQueue
        key: "/apps/ControlPanel/podqast/autoPlayNextInQueue"
        defaultValue: true
    }

    ConfigurationValue {
        id: episodeImageClickActionConf
        key: "/apps/ControlPanel/podqast/episodeImageClickAction"
        defaultValue: 0
    }

    function refresh() {
        connmanWifi.getProperties()
    }

    function tomillisecs(timestr) {
        // console.log(timestr)
        var a = timestr.substr(0, 8).split(":")
        var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2])
        var msecs = Number(timestr.substr(9, 11))
        // console.log("secs " + seconds + "milli" + msecs)
        var milli = msecs + seconds * 1000
        return milli
    }

    onIsInitializedChanged: if (isInitialized)
                                onConnectionTasks()

    DBusInterface {
        id: connmanWifi
        bus: DBus.SystemBus
        service: "net.connman"
        path: "/net/connman/technology/wifi" //<--- richtiger Pfad am Jolla
        iface: "net.connman.Technology"

        property bool wifiConnected

        signalsEnabled: true
        function propertyChanged(name, value) {
            //            console.log(name, value)
            if (name === "Connected") {
                wifiConnected = value
            }
        }

        function getProperties() {
            typedCall('GetProperties', undefined, function (result) {
                wifiConnected = result['Connected']
            })
        }
        Component.onCompleted: {
            onConnectionTasks()
        }
    }

    function onConnectionTasks() {
        var d = new Date()
        var seconds = Math.round(d.getTime() / 1000)
        if (podqast.isInitialized
                && seconds > lastRefreshed.value + refreshTimeConf.value) {
            feedparserhandler.refreshPodcasts()
        }

        connmanWifi.getProperties()
    }

    DBusAdaptor {
        service: "harbour.podqast.service"
        iface: "harbour.podqast.service"
        path: "/harbour/podqast/service"
        xml: "  <interface name=\"harbour.podqast.service\">\n"
             + "    <method name=\"openPage\"/>\n" + "  </interface>\n"

        function openPage(page, arguments) {
            if (page === "Inbox") {
                _showUpdatesNotification = false
            }
            __silica_applicationwindow_instance.activate()
            if (page === "ErrorPage"
                    || page !== pageStack.currentPage.objectName) {
                pageStack.clear()
                pageStack.replace(Qt.resolvedUrl("pages/%1.qml".arg(page)),
                                  arguments)
            }
        }
    }

    Notification {
        property string thepage: "Inbox"
        id: updatesNotification
        category: "x-podqast-updates"
        appIcon: "/usr/share/harbour-podqast/images/q.png"
        appName: "podQast"
        previewSummary: qsTr("New posts available")
        previewBody: qsTr("Click to view updates")
        body: qsTr("New Posts are available. Click to view.")
        remoteActions: [{
                "name": "default",
                "service": "harbour.podqast.service",
                "path": "/harbour/podqast/service",
                "iface": "harbour.podqast.service",
                "method": "openPage",
                "arguments": [thepage, {}]
            }]
    }

    Notification {
        id: appNotification
        category: "x-podqast-notify"
        appIcon: "/usr/share/harbour-podqast/images/q.png"
        // appIcon: "image://theme/icon-lock-application-update"
        appName: "podQast"
        previewSummary: qsTr("PodQast message")
        previewBody: qsTr("PodQast message")
        body: qsTr("New PodQast message")
    }

    Connections {
        target: feedparserhandler
        onUpdatesNotification: {
            console.log("we are on Notification", _showUpdatesNotification)
            if (_showUpdatesNotification) {
                updatesNotification.previewSummary = pctitle
                updatesNotification.previewBody = pstitle
                updatesNotification.body = pstitle
                updatesNotification.thepage = page
                updatesNotification.replacesId = new Date().getTime() / 1000
                updatesNotification.publish()
            } else {
                updatesNotification.close()
            }
        }
        onOpmlImported: {
            console.log("Notify the OPML imported")
            appNotification.previewSummary = opmlcount + qsTr(
                        " Podcasts imported")
            appNotification.previewBody = opmlcount + qsTr(
                        " Podcasts imported from OPML")
            appNotification.body = opmlcount + qsTr(
                        " Podcasts imported from OPML")
            appNotification.publish()
        }
    }

    Connections {
        target: queuehandler
        onNeedDownload: {
            if (doDownloadConf.value && (wifiConnected
                                         || doMobileDownConf.value)) {
                queuehandler.downloadAudio(podpost)
            }
        }
        onSetFirst: {
            playerHandler.setEpisode(data, chapterlist)
        }
    }

    Connections {
        target: externalhandler
        onAudioInfo: {
            console.log("audio info")
            ainfolabel = info
        }
    }

    FeedParserPython {
        id: feedparserhandler
    }

    QueueHandlerPython {
        id: queuehandler
    }

    ArchiveHandlerPython {
        id: archivehandler
    }

    ExternalHandlerPython {
        id: externalhandler
    }
    InboxHandlerPython {
        id: inboxhandler
    }

    GpodderNetPython {
        id: gpodderhandler
    }

    FyydDePython {
        id: fyydhandler
    }

    LogHandler {
        id: loghandler
    }

    PlayerHandler {
        id: playerHandler
        onAudioNotExist: {
            console.log("No audio exists")
            appNotification.previewSummary = qsTr("Error")
            appNotification.previewBody = qsTr("Audio File not existing")
            appNotification.body = qsTr("Audio File not existing")
            appNotification.publish()

            queuehandler.queueTopToArchive(autoPlayNextInQueue.value)
        }
    }
    FavoriteHandlerPython {
        id: favoritehandler
    }

    Item {
        PodqastAudioPlayer {
            id: mediaplayer

            onSourceChanged: console.log(
                                 "changing mediaplayer src to: " + mediaplayer.source)
        }

        Timer {
            id: mpristimer
            interval: 400
            repeat: false
            onTriggered: {
                interval = 10
                mprisPlayer.updateMetadata()
            }
        }

        MprisPlayer {
            id: mprisPlayer

            serviceName: "podqast"

            identity: "podQast"
            supportedUriSchemes: ["file", "http"]
            supportedMimeTypes: ["audio/x-vorbis-ogg", "audio/mpeg", "audio/mp4a-latm", "audio/x-aiff", "audio/ogg", "audio/opus"]

            canControl: true
            canGoNext: true
            canGoPrevious: true
            canPause: true
            canPlay: true
            canSeek: true
            playbackStatus: {
                switch (mediaplayer.playbackState) {
                case Audio.PlayingState:
                    return Mpris.Playing
                case Audio.PausedState:
                    return Mpris.Paused
                default:
                    return Mpris.Stopped
                }
            }
            rate: playerHandler.playrate

            metaData {
                url: mediaplayer.source
                duration: playerHandler.duration
                title: playerHandler.firsttitle
                contributingArtist: playerHandler.firstPodcastTitle
                artUrl: playerHandler.episode_logo_or_fallback
            }

            loopStatus: Mpris.LoopNone
            shuffle: false
            onPauseRequested: playerHandler.pause()
            onPlayPauseRequested: playerHandler.playpause()
            onStopRequested: playerHandler.stop()
            onPlayRequested: playerHandler.play()
            onNextRequested: playerHandler.fast_forward()
            onPreviousRequested: playerHandler.fast_backward()
        }
    }

    function to_pos_str(secs) {
        var date = new Date(null)
        date.setSeconds(secs)
        return date.toISOString().substr(11, 8)
    }
}
