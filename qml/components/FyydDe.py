# -*- coding: utf-8 -*-

import pyotherside
import threading
import sys


import urllib.request
import urllib.parse
import json


sys.path.append("/usr/share/harbour-podqast/python")


def search_pods(query):
    """
    Search for podcast list
    query: the search query
    """

    pclist = []

    params=urllib.parse.urlencode({"term": query})
    url = "https://api.fyyd.de/0.2/search/podcast?%s" % params

    response = urllib.request.urlopen(url)

    podcasts = json.load(response)['data']
    for podcast in podcasts:
        title = podcast['title']
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast['description']
        if len(description) > 160:
            description = description[:160] + "..."
        if not podcast['imgURL']:
            logo_url = ""
        else:
            logo_url = podcast['imgURL']

        pclist.append(
            {
                "url": podcast['xmlURL'],
                "title": title,
                "titlefull": podcast['title'],
                "description": description,
                "website": podcast['htmlURL'],
                "logo_url": logo_url,
            }
        )

    pyotherside.send("podsearch", pclist)


class FyydDe:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

    def searchpods(self, query):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=search_pods, args=[query])
        self.bgthread.start()


fyydde = FyydDe()
