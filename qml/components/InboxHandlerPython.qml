import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: inboxhandler

    signal inboxData(int offset, var data)
    signal getInboxPosts

    Component.onCompleted: {
        setHandler("inboxData", inboxData)
        setHandler("getInboxPosts", getInboxPosts)

        addImportPath(Qt.resolvedUrl('.'))
        importModule('InboxHandler', function () {
            console.log('InboxHandler is now imported')
        })
    }

    function getInboxEntries(podurl) {
        if (podurl === "home") {
            call("InboxHandler.inboxhandler.getinboxposts", function () {})
        } else {
            call("InboxHandler.inboxhandler.getinboxposts", [podurl],
                 function () {})
        }
    }
    function moveQueueTop(podpost) {
        call("InboxHandler.inboxhandler.movequeuetop", [podpost],
             function () {})
    }
    function moveQueueNext(podpost) {
        call("InboxHandler.inboxhandler.movequeuenext", [podpost],
             function () {})
    }
    function moveQueueBottom(podpost) {
        call("InboxHandler.inboxhandler.movequeuebottom", [podpost],
             function () {})
    }
    function moveArchive(podpost) {
        call("InboxHandler.inboxhandler.movearchive", [podpost], function () {})
    }
    function moveAllArchive(podpost) {
        call("InboxHandler.move_all_archive", function () {})
    }
}
