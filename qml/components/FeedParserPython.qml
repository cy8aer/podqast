import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: feedparserhandler

    signal feedInfo(var pcdata)
    signal podcastsList(var pcdata)
    signal podcastParams(var pcdata)
    signal subscribed(string pcurl)
    signal unsubscribed(string pcurl)
    signal updatesNotification(string pctitle, string pstitle, string page)
    signal refreshFinished
    signal htmlfile(string htmlfile)
    signal refreshProgress(real progress)
    signal refreshPost(string posttitle)
    signal opmlImported(int opmlcount)
    signal appError(string errmessage)
    signal refreshlimit(string podcasttitle)
    signal backupDone(string tarpath)
    signal opmlSaveDone(string opmlpath)
    signal episodeListData(var episodes, string offset, string totalcount)
    signal feedFetchError(string pod_url)

    Component.onCompleted: {
        setHandler("feedinfo", feedInfo)
        setHandler("podcastslist", podcastsList)
        setHandler("podcastParams", podcastParams)
        setHandler("subscribed", subscribed)
        setHandler("unsubscribed", unsubscribed)
        setHandler("updatesNotification", updatesNotification)
        setHandler("refreshFinished", refreshFinished)
        setHandler("htmlfile", htmlfile)
        setHandler("refreshProgress", refreshProgress)
        setHandler("refreshPost", refreshPost)
        setHandler("opmlimported", opmlImported)
        setHandler("apperror", appError)
        setHandler("refreshlimit", refreshlimit)
        setHandler("backupDone", backupDone)
        setHandler("opmlSaveDone", opmlSaveDone)
        setHandler("episodeListData", episodeListData)
        setHandler("feedFetchError", feedFetchError)

        addImportPath(Qt.resolvedUrl("./python"))
        addImportPath(Qt.resolvedUrl('.'))
        importModule('FeedParser', function () {
            console.log('FeedParser is now imported')

        })
    }

    function getPodcast(url) {
        call("FeedParser.instance.get_feedinfo", [url], function () {})
    }

    function getPodcastPreview(url, num_preview_episodes) {
        console.log("fetching preview for " + url)
        call("FeedParser.instance.get_podcast_preview",
             [url, num_preview_episodes], function () {})
    }
    function getPodcasts() {
        call("FeedParser.instance.get_podcasts", function () {})
    }

    function getEntries(search_options) {
        call("FeedParser.instance.get_entries", [search_options],
             function () {})
    }

    function subscribePodcast(url) {
        console.log("Subscribe url: " + url)
        call("FeedParser.instance.subscribe_podcast", [url], function () {})
    }
    function deletePodcast(url) {
        console.log("Delete url: " + url)
        call("FeedParser.instance.delete_podcast", [url], function () {})
    }
    function refreshPodcast(url) {
        console.log("Refresh url: " + url)
        _showUpdatesNotification = true
        call("FeedParser.instance.refresh_podcast",
             [url, moveToConf.value, doDownloadConf.value
              && (wifiConnected
                  || doMobileDownConf.value), autoLimitConf.value],
             function () {})
    }
    function refreshPodcasts(full) {
        if (migrationhandler.running)
            return
        if (full === undefined)
            full = false
        console.log("Refreshing all podcasts.")
        _showUpdatesNotification = true
        call("FeedParser.instance.refresh_podcasts",
             [moveToConf.value, doDownloadConf.value
              && (wifiConnected
                  || doMobileDownConf.value), autoLimitConf.value, Boolean(
                  full)], function () {})
        var d = new Date()
        var seconds = Math.round(d.getTime() / 1000)
        lastRefreshed.value = seconds
    }

    function getPodcastParams(url) {
        console.log("Get Podcast params")
        call("FeedParser.instance.get_podcast_params", [url], function () {})
    }
    function setPodcastParams(url, params) {
        console.log("Set Podcast Params")
        call("FeedParser.instance.set_podcast_params", [url, params],
             function () {})
    }

    function renderHtml(data) {
        console.log("Set Podcast Params")
        call("FeedParser.instance.render_html", [data], function () {})
    }

    function nomedia(doset) {
        console.log("Set Podcast Params")
        call("FeedParser.instance.nomedia", [doset], function () {})
    }
    function importOpml(opmlfile) {
        console.log("Import Podcasts from OPML")
        call("FeedParser.instance.import_opml", [opmlfile], function () {})
    }
    function importGpodder() {
        console.log("Import Podcasts from Gpodder database")
        call("FeedParser.instance.import_gpodder", function () {})
    }
    function doBackup(withStore) {
        console.log("Backup")
        call("FeedParser.instance.do_backup", [withStore], function () {})
    }
    function doWriteOpml() {
        console.log("Writing opml file")
        call("FeedParser.instance.write_opml", function () {})
    }
    function moveArchive(id) {
        call("FeedParser.instance.move_archive", [id], function () {})
    }

    onError: {
        console.log('python error: ' + traceback)
    }

    onRefreshlimit: {
        appNotification.previewSummary = qsTr("Auto-Post-Limit reached")
        appNotification.previewBody = qsTr("for %1").arg(podcasttitle)
        appNotification.body = qsTr("Auto-Post-Limit reached for %1").arg(
                    podcasttitle)
        appNotification.replacesId = "limitNotification"
        appNotification.publish()
    }

    onAppError: {
        console.log("Notify error " + errmessage)
        appNotification.previewSummary = qsTr("Error")
        appNotification.previewBody = errmessage
        appNotification.body = errmessage
        appNotification.publish()
    }
}
