import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property var url
    property var title
    property var titlefull
    property var description
    property var website
    property var logo_url
    property var altdata

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        VerticalScrollDecorator {}
        contentHeight: page.height
        Column {
            id: titlecolumn
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                id: pageheader
                title: titlefull
            }
        }

        PrefAboutMenu {}

        Component.onCompleted: {
            console.log("loading preview for feed '" + url + "'")
            feedparserhandler.getPodcastPreview(url, 3)
        }

        Connections {
            target: feedparserhandler

            onFeedInfo: {
                url = pcdata.url
                console.debug("feedinfo for " + url)
                podimage.source = pcdata.logo_url
                poddescription.text = pcdata.description
                description = pcdata.descriptionfull
                titlefull = pcdata.title
                subscribebutton.enabled = true
                altdata = pcdata.altfeeds
                altcombo.visible = false
                if (pcdata.altfeeds.length === 0) {
                    console.debug("no alternative or wrong mime type")
                } else if (pcdata.altfeeds.length === 1) {
                    console.debug(
                                "we have one alternative: " + pcdata.altfeeds[0].url)
                } else {
                    console.debug("we have more than one alternative: ")
                    altpods.clear()

                    for (var i = 0; i < pcdata.altfeeds.length; i++) {
                        console.debug(pcdata.altfeeds[i].title)
                        altpods.append(pcdata.altfeeds[i])
                    }
                    altcombo.visible = true
                }
                lpostlabel.visible = true
                lentryListModel.clear()
                pcdata.latest_entries.forEach(function (preview_entry) {
                    lentryListModel.append(preview_entry)
                })
            }

            onFeedFetchError: {
                if (pod_url === url)
                    poddescription.text = qsTr(
                                "Could not load feed %1").arg(url)
            }

            onSubscribed: {
                subscribebutton.subscribed = true
                subscribebutton.subscribing = false
                subscribebutton.enabled = true
            }
        }

        Row {
            id: inforow
            height: parent.width / 2.5
            width: parent.width
            anchors.margins: 10
            anchors.top: titlecolumn.bottom
            MouseArea {
                width: parent.width
                height: parent.height
                onClicked: pageStack.push(Qt.resolvedUrl("Poddescription.qml"),
                                          {
                                              "description": description,
                                              "title": titlefull
                                          })
                Column {
                    id: imagecolumn
                    // anchors.left: parent.left
                    width: parent.width / 2.5 - 10
                    height: parent.width / 2.5 - 10

                    Image {
                        id: podimage
                        asynchronous: true
                        source: logo_url
                        height: parent.width - 20
                        width: parent.width - 20
                        signal failedToLoad

                        onStatusChanged: {
                            if (status == Image.Error) {
                                source = "../../images/podcast.png"
                                failedToLoad()
                            }
                        }
                        BusyIndicator {
                            size: BusyIndicatorSize.Large
                            anchors.centerIn: podimage
                            running: podimage.status != Image.Ready
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                pageStack.push(Qt.resolvedUrl(
                                                   "../pages/PodpostList.qml"),
                                               {
                                                   "url": url
                                               })
                            }
                        }
                    }
                }
                Column {
                    anchors.left: imagecolumn.right
                    anchors.right: parent.right
                    Label {
                        id: poddescription
                        width: parent.width
                        height: parent.height
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        text: description
                        font.pixelSize: Theme.fontSizeExtraSmall
                    }
                }
            }
        }
        Row {
            width: parent.width
            anchors.top: inforow.bottom
            id: podinfo

            Button {
                id: subscribebutton

                property bool subscribed: false
                property bool subscribing: false

                width: parent.width / 3.5
                text: subscribed ? qsTr("Configure") : qsTr("Subscribe")
                onClicked: {
                    if (subscribed == false) {
                        do_subscribe()
                    } else {
                        pageStack.push(Qt.resolvedUrl("PodcastSettings.qml"), {
                                           "podtitle": title,
                                           "url": url
                                       })
                    }
                }

                enabled: false

                function do_subscribe() {
                    if (altdata.length > 1) {
                        console.log("combo value: " + altcombo.value)
                        for (var i = 0; i < altdata.length; i++) {
                            if (altdata[i].title === altcombo.value) {
                                url = altdata[i].url
                            }
                        }
                    }
                    console.log(url)
                    subscribing = true
                    enabled = false
                    feedparserhandler.subscribePodcast(url)
                }

                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: subscribebutton
                    running: subscribebutton.subscribing
                             || !subscribebutton.enabled
                }
            }
            ComboBox {
                id: altcombo
                visible: false
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Feed alternatives")
                    }
                    Repeater {
                        model: ListModel {
                            id: altpods
                        }
                        delegate: MenuItem {
                            text: title
                            font.pixelSize: Theme.fontSizeSmall
                        }
                    }
                }
            }
        }
        Row {
            id: lpostlabel
            height: Theme.itemSizeExtraSmall
            anchors.top: podinfo.bottom
            visible: false
            Label {
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                text: qsTr("Latest Post")
            }
        }

        SilicaListView {
            id: fpodentries
            clip: true
            anchors.top: lpostlabel.bottom
            anchors.bottom: parent.bottom
            width: parent.width

            model: ListModel {
                id: lentryListModel
            }
            delegate: PodcastPreviewPodpostListItem {}
        }
        PlayDockedPanel {}
    }
}
