import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    allowedOrientations: Orientation.All
    property int margin: Theme.paddingMedium

    SilicaFlickable {
        id: mainflick
        anchors.fill: parent

        PrefAboutMenu {}

        Column {
            id: column
            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Export")
            }

            BackupButtons {
               width: page.width
               anchors {
                   leftMargin: Theme.horizontalPageMargin
                   rightMargin: Theme.horizontalPageMargin
               }
            }
        }

        PlayDockedPanel { }
    }
}
