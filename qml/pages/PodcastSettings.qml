import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    property var url
    property var podtitle
    property var feedinfo

    DialogHeader {
        id: header
        title: qsTr("Settings")
    }

    SilicaFlickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: Theme.paddingMedium
        }
        contentWidth: parent.width - 2 * Theme.paddingMedium

        VerticalScrollDecorator {}

        Column {
            id: downloadConf
            width: parent.width

            Label {
                text: podtitle
                font.pixelSize: Theme.fontSizeLarge
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                width: parent.width
            }

            Label {
                text: qsTr("%1 Posts").arg(feedinfo["episodecount"])
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }

            Label {
                text: qsTr("URL")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }

            LinkedLabel {
                plainText: url
                wrapMode: Text.WrapAnywhere
                width: parent.width
            }

            Label {
                text: qsTr("Link")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
                visible: linklabel.visible
            }

            LinkedLabel {
                id: linklabel
                plainText: feedinfo["link"]
                wrapMode: Text.WrapAnywhere
                visible: plainText.length > 0
                width: parent.width
            }
            Item {
                height: Theme.itemSizeLarge
                width: parent.width
                Separator {
                    anchors.centerIn: parent
                    color: Theme.highlightColor
                    width: parent.width
                }
            }

            ComboBox {
                id: moveTo
                width: parent.width
                label: qsTr("Move new post to")
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Inbox")
                    }
                    MenuItem {
                        text: qsTr("Top of Playlist")
                    }
                    MenuItem {
                        text: qsTr("Bottom of Playlist")
                    }
                    MenuItem {
                        text: qsTr("Archive")
                    }
                }
            }
            Slider {
                id: podAutoLimit
                width: parent.width
                label: qsTr("Automatic post limit")
                minimumValue: 0
                maximumValue: 10
                handleVisible: true
                valueText: value == 0 ? "All" : value
                stepSize: 1
            }
            Slider {
                id: playrate
                enabled: false
                visible: false
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                width: parent.width
                label: qsTr("Audio playrate")
                minimumValue: 0.85
                maximumValue: 2.0
                handleVisible: true
                valueText: "1:" + value
                stepSize: 0.15
            }
        }
    }
    onOpened: {
        console.log("Title: " + podtitle)
        feedparserhandler.getPodcastParams(url)
        feedparserhandler.getPodcast(url)
    }

    onAccepted: {
        var data = {
            "move": moveTo.currentIndex,
            "autolimit": podAutoLimit.value,
            "playrate": playrate.value
        }
        feedparserhandler.setPodcastParams(url, data)
    }

    Connections {
        target: feedparserhandler
        onPodcastParams: {
            if (pcdata.move !== -1) {
                moveTo.currentIndex = pcdata.move
            } else {
                moveTo.currentIndex = moveToConf.value
            }
            if (pcdata.autolimit) {
                podAutoLimit.value = pcdata.autolimit
            } else {
                podAutoLimit.value = autoLimitConf.value
            }

            if (pcdata.playrate !== 0) {
                playrate.value = pcdata.playrate
            } else {
                playrate.value = globalPlayrateConf.value
            }
        }
        onFeedInfo: {
            feedinfo = pcdata
        }
    }
}
