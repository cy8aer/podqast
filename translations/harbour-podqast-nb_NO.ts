<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="45"/>
        <source>Manage Podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="53"/>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="61"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="70"/>
        <source>External Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="83"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="84"/>
        <source>Collecting Podcasts</source>
        <translation>Samler nettradioopptak…</translation>
    </message>
</context>
<context>
    <name>BackupButtons</name>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="17"/>
        <source>Backup done</source>
        <translation>Sikkerhetskopi utført</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="18"/>
        <location filename="../qml/components/BackupButtons.qml" line="19"/>
        <source>Backup done to </source>
        <translation>Sikkerhetskopi lagret i </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="26"/>
        <source>OPML file saved</source>
        <translation>OPML-fil lagret</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="27"/>
        <location filename="../qml/components/BackupButtons.qml" line="28"/>
        <source>OPML file saved to </source>
        <translation>OPML-fil lagret i </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="36"/>
        <source>Backup</source>
        <translation>Sikkerhetskopi</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="52"/>
        <source>Save to OPML</source>
        <translation>Lagre som OPML</translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="49"/>
        <source>Chapters</source>
        <translation>Kapitler</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="108"/>
        <source>No chapters</source>
        <translation>Ingen kapitler</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="109"/>
        <source>Rendering chapters</source>
        <translation>Genererer kapitler …</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation> kapitler</translation>
    </message>
</context>
<context>
    <name>DataMigration</name>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="16"/>
        <source>Update done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="17"/>
        <source>Successfully migrated data to the current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="41"/>
        <source>Data Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="73"/>
        <source>Because the data format changed, podqast will need to migrate your data to the new format. You can use one of the buttons below to create a backup of your data for recovery.</source>
        <extracomment>general text explaining what the migration does</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="80"/>
        <source>&lt;b&gt;Whats New?&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Some of you lost episodes in the last migration. Because we found this pretty sad, we&apos;re trying to recover them now.&lt;/li&gt;&lt;li&gt;If you are missing old episodes of feeds in general and this didn&apos;t help, try the force refresh button in the settings (We added support for Pagination).&lt;/li&gt;&lt;li&gt;Podcasts are now sorted alphabetically, we hope you like that!&lt;/li&gt;&lt;li&gt;Gpodder should work again.&lt;/li&gt;&lt;li&gt;Check the new Log Page next to the settings.&lt;/li&gt;&lt;li&gt;If you&apos;re streaming an episode and the download finishes, we&apos;re now switching to the downloaded file automatically.&lt;/li&gt;&lt;li&gt;Also more speed, bugfixes and code cleanups.&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;If you want to contribute to podQast, you can help translating the app or report issues on GitLab.</source>
        <extracomment>whatsnew section of the migration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="87"/>
        <source>The migration is running, please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="99"/>
        <source>Start Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="125"/>
        <source>Try to ignore errors during migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="137"/>
        <source>We couldnt migrate your data completly. You can restart the app and try it in the non-strict mode. Please consider reporting this as a bug in the Issue-Tracker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="143"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="148"/>
        <source>Open issue tracker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="13"/>
        <source>Discover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="26"/>
        <source>Search on gpodder.net</source>
        <translation>Søk på gpodder.net</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="30"/>
        <source>Search on fyyd.de</source>
        <translation>Søk på fyyd.de</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="34"/>
        <source>Tags...</source>
        <translation>Etiketter …</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="38"/>
        <source>Url...</source>
        <translation type="unfinished">Nettadresse …</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="42"/>
        <source>Import...</source>
        <translation type="unfinished">Importer …</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="46"/>
        <source>Export...</source>
        <translation type="unfinished">Eksporter …</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="24"/>
        <source>Export</source>
        <translation>Eksporter</translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="23"/>
        <source> Podcasts imported</source>
        <translation type="unfinished"> nettradioopptak importert</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation> Nettradioopptak importert fra OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="46"/>
        <source>Discover by Importing</source>
        <translation>Oppdag ved import</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="59"/>
        <source>Pick an OPML file</source>
        <translation>Velg en OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="60"/>
        <source>OPML file</source>
        <translation>Importer OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="80"/>
        <source>Import OPML</source>
        <translation>Importer OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="82"/>
        <source>Import OPML File</source>
        <translation>Importer OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="102"/>
        <source>Import from Gpodder</source>
        <translation>Importer fra Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="104"/>
        <source>Import Gpodder Database</source>
        <translation type="unfinished">Importer Gpodder-database</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="128"/>
        <source>Please note: Importing does take some time!</source>
        <translation>Merk: Importering tar lang tid.</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="42"/>
        <source>Discover Tags</source>
        <translation>Oppdag etiketter</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="26"/>
        <source>Discover by URL</source>
        <translation>Oppdag via nettadresse</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="46"/>
        <source>Enter podcasts&apos; feed url</source>
        <translation type="unfinished">Skriv inn nettradioopptakets nettadresse</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="64"/>
        <source>Please enter feed urls here - not web page urls</source>
        <translation>Skriv inn strømmingsnettadresser her, ikke nettside-nettadresser</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="77"/>
        <source>Discover</source>
        <translation>Oppdag</translation>
    </message>
</context>
<context>
    <name>EpisodeContextMenu</name>
    <message>
        <location filename="../qml/components/EpisodeContextMenu.qml" line="96"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <location filename="../qml/pages/External.qml" line="49"/>
        <source>External Audio</source>
        <translation>Ekstern lyd</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="66"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="67"/>
        <source>Collecting Posts</source>
        <translation>Innhenting av poster</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="46"/>
        <source>Favorites</source>
        <translation>Favoritter</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="149"/>
        <source>Auto-Post-Limit reached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="150"/>
        <source>for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="151"/>
        <source>Auto-Post-Limit reached for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="159"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="45"/>
        <source>History</source>
        <translation>Historikk</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="62"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="63"/>
        <source>Collecting Posts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="50"/>
        <source>Moving all posts to archive</source>
        <translation>Flytter alle poster til arkiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="78"/>
        <source>Clear Inbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="94"/>
        <source>No new posts</source>
        <translation>Ingen nye poster</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="95"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../qml/pages/Log.qml" line="25"/>
        <source>Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="63"/>
        <source>No logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="74"/>
        <source>Skipped refresh on &apos;%1&apos;, because the server responded with &apos;Not Modified&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="77"/>
        <source>Updated &apos;%1&apos;. %2 new episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="80"/>
        <source>&apos;%1&apos; could not be refreshed because: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainCarousel</name>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="33"/>
        <source>Inbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="33"/>
        <source>Playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="33"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="66"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="71"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainCarousel.qml" line="75"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPageUsageHint</name>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="63"/>
        <source>You can see whats going on by looking into the log down here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="68"/>
        <source>Podqast supports pagination of feeds. If you are missing old episodes of feeds, you can find a force refresh button in the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="21"/>
        <source>On an episode image, this icon shows that it is downloaded...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="9"/>
        <source>Hi there, I gonna give you a quick tour to show you basic and new features.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="15"/>
        <source>Quick touch on an episode to see its description, long press it to add it to queue or play it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="28"/>
        <source>...currently playing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="35"/>
        <source>...already listened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="42"/>
        <source>In the dock below, this icon means the file currently played is streaming from the internet right now...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="49"/>
        <source>...the sleeptimer is active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="56"/>
        <source>...a playrate different from 1 is set.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="16"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="16"/>
        <source>Stop after each episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="53"/>
        <source>Audio playrate</source>
        <translation>Lyd-avspillingstakt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="90"/>
        <source>Sleep timer</source>
        <translation>Søvntidsur</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="90"/>
        <source> running...</source>
        <translation> kjører …</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="213"/>
        <source> chapters</source>
        <translation> kapitler</translation>
    </message>
</context>
<context>
    <name>PlayerHint</name>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="8"/>
        <source>Select if the next item of the playlist should be played automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="13"/>
        <source>Click the image to adjust the playrate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastDirectorySearchPage</name>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="27"/>
        <source>Discover by Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="32"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="56"/>
        <source>Loading results for &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="60"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="24"/>
        <source>Refresh</source>
        <translation>Gjenoppfrisk</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="32"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="44"/>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="50"/>
        <source>Deleting Podcast</source>
        <translation>Sletter nettradioopptak …</translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="45"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="57"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>%1 Posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="83"/>
        <source>Move new post to</source>
        <translation>Flytt ny post til</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="86"/>
        <source>Inbox</source>
        <translation>Innboks</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="89"/>
        <source>Top of Playlist</source>
        <translation>Toppen av spillelisten</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="92"/>
        <source>Bottom of Playlist</source>
        <translation>Bunnen av spillelisten</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="95"/>
        <source>Archive</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="102"/>
        <source>Automatic post limit</source>
        <translation>Automatisk post-grense</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="119"/>
        <source>Audio playrate</source>
        <translation>Lyd-avspillingstakt</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="30"/>
        <source>Search by Tag...</source>
        <translation>Søk etter etikett …</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="107"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="108"/>
        <source>Creating items</source>
        <translation>Opprettelse av elementer</translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="27"/>
        <source>Open in browser</source>
        <translation>Åpne i nettleser</translation>
    </message>
</context>
<context>
    <name>PostListItem</name>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="105"/>
        <source>playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="109"/>
        <source>listened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="113"/>
        <source>remaining</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="8"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="12"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="17"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="22"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="54"/>
        <source>No posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="55"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RefreshPodcastsPulley</name>
    <message>
        <location filename="../qml/components/RefreshPodcastsPulley.qml" line="11"/>
        <source>refresh podcasts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/RefreshPodcastsPulley.qml" line="24"/>
        <source>refreshing: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>When touching episode image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="51"/>
        <source>Show Podcast Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="73"/>
        <source>Move new post to</source>
        <translation>Flytt ny post til</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="76"/>
        <location filename="../qml/pages/Settings.qml" line="231"/>
        <source>Inbox</source>
        <translation>Innboks</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <location filename="../qml/pages/Settings.qml" line="240"/>
        <source>Archive</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Automatic post limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="123"/>
        <source>Refresh time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="140"/>
        <source>Audio playrate</source>
        <translation>Lyd-avspillingstakt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="127"/>
        <source>off</source>
        <translation>av</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="156"/>
        <source>Time left to ignore for listened-mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="251"/>
        <source>Download/Streaming</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="342"/>
        <source>Development</source>
        <translation>Utvikling</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="353"/>
        <source>Experimental features</source>
        <translation>Eksperimentelle funksjoner</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="79"/>
        <location filename="../qml/pages/Settings.qml" line="234"/>
        <source>Top of Playlist</source>
        <translation>Toppen av spillelisten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <source>Play Episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="54"/>
        <source>Show Episode Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="57"/>
        <source>Add To Queue Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="60"/>
        <source>Add To Queue Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <location filename="../qml/pages/Settings.qml" line="237"/>
        <source>Bottom of Playlist</source>
        <translation>Bunnen av spillelisten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="101"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="160"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="172"/>
        <source>Sleep timer</source>
        <translation>Søvntidsur</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="176"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="195"/>
        <source>External Audio</source>
        <translation>Ekstern lyd</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="207"/>
        <source>Allow external audio</source>
        <translation>Tillat ekstern lyd</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="215"/>
        <source>Will take data from
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="228"/>
        <source>Move external audio to</source>
        <translation>Flytt ekstern lyd til</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>Download Playlist Posts</source>
        <translation>Last ned spillelisteposter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="277"/>
        <source>Download on Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="286"/>
        <source>Keep Favorites downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="295"/>
        <source>Will save data in
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="323"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="334"/>
        <source>Audio viewable in system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="186"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="362"/>
        <source>Force refresh of old episodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubscribePodcast</name>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="78"/>
        <source>Could not load feed %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="167"/>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="167"/>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="208"/>
        <source>Feed alternatives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="231"/>
        <source>Latest Post</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="30"/>
        <source>../data/about.txt</source>
        <translation type="unfinished">../data/about-nb.txt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="48"/>
        <source>Next</source>
        <translation>Neste</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="36"/>
        <source>Let&apos;s start...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="37"/>
        <source>Back to info</source>
        <translation>Tilbake til info</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="54"/>
        <source> Podcasts imported</source>
        <translation> Nettradioopptak importert</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="55"/>
        <location filename="../qml/pages/Wizzard2.qml" line="56"/>
        <source> Podcasts imported from OPML</source>
        <translation> Nettradioopptak importert fra OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="73"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="102"/>
        <source>Pick an OPML file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="103"/>
        <source>OPML file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="125"/>
        <source>Import OPML</source>
        <translation>Importer OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="127"/>
        <source>Import OPML File</source>
        <translation>Importer OPML-fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="146"/>
        <source>Import from Gpodder</source>
        <translation>Importer fra Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="149"/>
        <source>Import Gpodder Database</source>
        <translation>Importer Gpodder-database</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="175"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="356"/>
        <source>New posts available</source>
        <translation>Nye poster tilgjengelig</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="357"/>
        <source>Click to view updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="358"/>
        <source>New Posts are available. Click to view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="375"/>
        <location filename="../qml/harbour-podqast.qml" line="376"/>
        <source>PodQast message</source>
        <translation>PodQast-melding</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="377"/>
        <source>New PodQast message</source>
        <translation>Ny PodQast-melding</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="397"/>
        <source> Podcasts imported</source>
        <translation> Nettradioopptak importert</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="399"/>
        <location filename="../qml/harbour-podqast.qml" line="401"/>
        <source> Podcasts imported from OPML</source>
        <translation> Nettradioopptak importert fra OPML</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="463"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="464"/>
        <location filename="../qml/harbour-podqast.qml" line="465"/>
        <source>Audio File not existing</source>
        <translation>Lydfilen finnes ikke</translation>
    </message>
</context>
</TS>
